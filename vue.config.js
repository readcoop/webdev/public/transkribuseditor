const webpack = require("webpack");
module.exports = {
  configureWebpack: {
    plugins: [
      new webpack.BannerPlugin(
        "Copyright (C) READ-COOP SCE - All Rights Reserved"
      ),
      // new PostCSSPrefix({
      //   input:'./src/assets/custom.scss',
      //   output: './public/style-prefix.scss',
      //   prefixProperties: {
      //     prefix: 'transkribusEditor-',

      //   }
      // })
    ],
    devtool: "source-map",
    // module: {
    //   rules: [
    //     {
    //       test: /\.js$/,
    //       exclude: /(node_modules)/,
    //       use: {
    //         loader: "babel-loader",
    //         options: {
    //           presets: ["@babel/preset-env"],
    //         },
    //       },
    //     },
    //     {
    //       test: /\.s(c|a)ss$/,
    //       use: {
    //         loader: "css-loader",
    //       }, // Interprets scss @import and url() like import/require()
    //     },
    //     {
    //       test: /\.s(c|a)ss$/,
    //       exclude: [/custom/],
    //       use: {
    //         loader: "postcss-loader",
    //       },
    //     }, // PostCSS turns your SCSS file into a JS object & converts that object back to a SCSS file
    //     {
    //       test: /\.s(c|a)ss$/,
    //       use: {
    //         loader: "sass-loader",
    //       }, // look for scss file through sass-loader, compile scss into css code
    //     },
    //   ],
    // },
  },
};
