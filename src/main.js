/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import Vue from "vue";
import App from "./App.vue";
import Vuex from "vuex";
import Buefy from "buefy";

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import fontawesome from "@/plugins/fontawesome.js";
import VueFullscreen from "vue-fullscreen";
import banner from "rollup-plugin-banner";
import store from "@/store";

Vue.config.productionTip = false;

Vue.use(Vuex);
Vue.use(fontawesome);

Vue.use(Buefy, {
  defaultIconComponent: FontAwesomeIcon,
  defaultIconPack: "fas",
});
Vue.use(VueFullscreen);

const root_element = document.getElementById("transkribusEditor");
const AppRoot = Vue.extend(App);


window.eventBus = new AppRoot({
  store,
  el: root_element,
  propsData: { ...root_element.dataset },
  mounted() {
    this.$root.$on("save", this.save);
    const that = this;
    this.$root.$on("change", function (payLoad) {
      if (payLoad.iiifUrl) {
        that.$store.dispatch("content/setIIIF", {
          iiifUrl: payLoad.iiifUrl,
        });
      }
      if (payLoad.xmlJsonString) {
        var cleanedString = this.escapeSpecialChars(payLoad.xmlJsonString);
        const obj = JSON.parse(cleanedString);
        that.$store.dispatch("content/setXmlJson", {
          xmlJson: obj,
        });
      }
      if (payLoad.xml) {
        that.$store.dispatch("content/setXml", {
          xml: payLoad.xml,
        });
      }
    });
  },
  methods: {
    save(value) {
      window.output = value;
      root_element.dataset.output = JSON.stringify(value);
      window.eventBus.$emit("dataSaved", value);
    },
    escapeSpecialChars(string) {
      return string
        .replace(/\\n/g, "\\n")
        .replace(/\\'/g, "\\'")
        .replace(/\\"/g, '\\"')
        .replace(/\\&/g, "\\&")
        .replace(/\\r/g, "\\r")
        .replace(/\\t/g, "\\t")
        .replace(/\\b/g, "\\b")
        .replace(/\\f/g, "\\f");
    },
  },
}).$mount("#transkribusEditor");

export default AppRoot;
