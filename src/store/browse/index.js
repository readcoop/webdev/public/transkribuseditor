/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import deepEqual from 'fast-deep-equal'
const tags = require('../../assets/tag-config.json');

export const state = () => ({
  docId: null,
  title: null,
  colTitle: null,
  colRole: null,
  collections: [],
  tagDefs: tags,
  documents: [],
})


export const mutations = {
  update(state, params) {
    for (const key in params)
      if (!deepEqual(state[key], params[key])) state[key] = params[key]
  }
}

export const actions = {
  update({ commit }, params) {
    commit('update', params)
  },
  setCollections({ dispatch, state }, { collections }) {
    dispatch('update', { collections: collections })
  },
  setTagDefs({ dispatch, state }, { tagDefs }) {
    dispatch('update', { tagDefs: tagDefs })
  },
  setDocuments({ dispatch, state }, { documents }) {
    dispatch('update', { documents: documents })
  },
  setColTitle({ dispatch, state }, { colTitle }) {
    dispatch('update', { colTitle: colTitle })
  },
  setColRole({ dispatch, state }, { colRole }) {
    dispatch('update', { colRole: colRole })
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions,
};