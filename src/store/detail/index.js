/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import {
  fetchDocumentsMeta,
  fetchPageInfo,
  fetchSearchResults
} from '@/utils/api.js'

const DEFAULT = {
  // document
  docId: null,
  title: null,
  colTitle: null,
  maxPage: NaN,
  documentInfo: null,
  isDocumentReady: false,
  isDocumentNotFoundError: false,
  // page
  pageId: null,
  pageNum: NaN,
  pageInfo: null,
  pageContentId: null,
  tsList: null,
  tsKey: null,
  // content: null,
  image: null,
  isPageReady: false,
  isPageContentReady: false,
  isPageNotFoundError: false,
  isPageContentNotFoundError: false,
  // search
  term: '',
  results: [],
  // shared
  requestCount: 0,
  // view
  view: 'both',
  tagMode: 'hidetags',
  changed: false
}

export const state = () => Object.assign({}, DEFAULT)

export const getters = {
  isLoading: (state, getters, rootState) =>
    state.requestCount > 0 && state.content.isLoading === true,
  hasResults: state => state.results.length > 0
}

export const mutations = {
  update(state, params) {
    for (const key in params) {
      if (state[key] !== params[key]) {
        state[key] = params[key]
        state.changed = true
      }
    }
  },
  beginRequest(state) {
    state.requestCount += 1
  },
  endRequest(state) {
    if (state.requestCount > 0) state.requestCount -= 1
  }
}

export const actions = {
  update({ commit, state }, params = {}) {
    const o = {}
    for (const key in params) if (key in state) o[key] = params[key]
    commit('update', o)
  },
  async fetch({ dispatch, state }, routeParams) {
    dispatch('beginRequest')
    // if (state.view !== view) await dispatch('update', { view })
    if (routeParams.docid !== state.docId) {
      // await dispatch('update', { documentId, pageNum, results: [] })
      await dispatch('fetchDocumentInfo', {
        collId: routeParams.collectionid,
        docId: routeParams.docid
      })
      await dispatch('fetchPageInfo', {
        collId: routeParams.collectionid,
        docId: routeParams.docid,
        pageNr: routeParams.pageid
      })
      //     if (term) {
      // dispatch('update', { term })
      // await dispatch('fetchSearchResults')
      //     }
    }
    // else if (pageNum !== state.pageNum) {
    else {
      await dispatch('update', { pageNum, results: [] })
      await dispatch('fetchPageInfo', {
        collId: routeParams.collectionid,
        docId: routeParams.docid,
        pageNr: routeParams.pageid
      })
      if (term) {
        dispatch('update', { term })
        await dispatch('fetchSearchResults')
      }
    }
    // else  if (term && state.term !== term) {
    //   dispatch('update', { term })
    //   await dispatch('fetchSearchResults')
    // }
    dispatch('endRequest')
  },
  beginRequest({ commit }) {
    commit('beginRequest')
  },
  endRequest({ commit }) {
    commit('endRequest')
  },
  async fetchDocumentInfo({ state, dispatch }, { collId, docId }) {
    dispatch('beginRequest')
    const data = await fetchDocumentsMeta({ collId: collId, docId: docId })

    if (data === null) {
      dispatch('update', {
        isDocumentNotFoundError: true
      })
    } else {
      const colTitle = data.collectionList.colList.find(
        element => element.colId == collId
      )
      dispatch('update', {
        docId: data.docId,
        maxPage: data.nrOfPages,
        isDocumentReady: true,
        title: data.title,
        colTitle: colTitle
      })
    }
    dispatch('endRequest')
  },
  async fetchPageInfo({ state, dispatch }, { collId, docId, pageNr }) {
    dispatch('beginRequest')
    const data = await fetchPageInfo({
      collId: collId,
      docId: docId,
      pageNr: pageNr
    })
    if (data === null)
      dispatch('update', {
        image: null,
        isPageReady: false,
        isPageNotFoundError: true
      })
    else {
      let image = { id: data.key, width: data.width, height: data.height }
      dispatch('update', {
        ...data.id,
        image: image,
        pageNum: data.pageNr,
        isPageReady: true,
        tsList: data.tsList,
        tsKey: data.tsList.transcripts[0].key
      })
      // dispatch('content/update', { id: data.content.id })
      dispatch('fetchPageContent', { id: data.tsList.transcripts[0].key })
    }
    dispatch('endRequest')
  },
  async fetchPageContent({ state, dispatch }, { id }) {
    dispatch('update', {
      tsKey: id
    })
    await dispatch('content/fetch', { id })
  },
  async fetchSearchResults({ state, dispatch }) {
    dispatch('beginRequest')
    const data = await fetchSearchResults(
      {
        term: state.term,
        page: state.pageId
      },
      { offset: 0, limit: 100 }
    )

    if (data === null) {
      dispatch('update', { results: [] })
    } else {
      dispatch('update', {
        results:
          data.results.length === 0
            ? Object.freeze([])
            : Object.freeze(data.results[0].results)
      })
    }
    dispatch('endRequest')
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};