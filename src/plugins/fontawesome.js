/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faSearch,
  faAngleRight,
  faAngleLeft,
  faAngleUp,
  faAngleDown,
  faMinusSquare,
  faPlusSquare,
  faChevronLeft,
  faChevronRight,
  faChevronUp,
  faChevronDown,
  faFileAlt,
  faTimes,
  faArrowUp,
  faArrowDown,
  faPlus,
  faMinus,
  faTrash,
  faDesktop,
  faColumns,
  faImage,
  faCode,
  faEye,
  faPaperPlane,
  faCheck,
  faShoppingCart,
  faDownload,
  faEyeSlash,
  faTags,
  faCaretDown,
  faCaretUp,
  faImages,
  faAlignLeft,
  faObjectGroup,
  faQuestion,
  faQuestionCircle,
  faExternalLinkAlt,
  faUndo,
  faRedo,
  faBars,
  faSave,
  faBold,
  faCompress,
  faExpand,
  faStrikethrough,
  faUnderline,
  faSubscript,
  faSuperscript,
  faTag,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

export default {
  install(Vue, options = {}) {
    library.add(
      faSearch,
      faAngleRight,
      faAngleLeft,
      faAngleUp,
      faAngleDown,
      faMinusSquare,
      faPlusSquare,
      faChevronLeft,
      faChevronRight,
      faChevronUp,
      faChevronDown,
      faFileAlt,
      faTimes,
      faArrowUp,
      faCompress,
      faExpand,
      faArrowDown,
      faPlus,
      faMinus,
      faTrash,
      faDesktop,
      faColumns,
      faImage,
      faCode,
      faEye,
      faPaperPlane,
      faCheck,
      faShoppingCart,
      faDownload,
      faEyeSlash,
      faTags,
      faCaretDown,
      faCaretUp,
      faAlignLeft,
      faImages,
      faObjectGroup,
      faQuestion,
      faQuestionCircle,
      faExternalLinkAlt,
      faUndo,
      faRedo,
      faBars,
      faSave,
      faBold,
      faStrikethrough,
      faUnderline,
      faSubscript,
      faSuperscript,
      faTag
    );

    Vue.component("font-awesome-icon", FontAwesomeIcon);
  },
};
