/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

export default {
  // imageBaseUrl: 'https://dbis-thure.uibk.ac.at/iiif/2',
  // imageBaseUrl: 'https://files-test.transkribus.eu/iiif/2',
  urls: {
    image: "https://files.transkribus.eu/iiif/2",
    xml: "https://files.transkribus.eu/Get?id=",
  },
  hashidsSecret: "1234",
  token: "",
  transcriptionURL: "https://transkribus.eu/r/testing/sandbox/application/",
  searchApiBaseUrl: "https://transkribus.eu/r/zQPlDb1ZvVWJuiBU/api/v0",
  apiBaseUrl: "https://transkribus.eu/TrpServerTesting/rest",
  contactEmail: "info@readcoop.eu",
  bugEmail: "bugs@readcoop.eu",
  bugtrackerUrl:
    "incoming+readcoop-webdev-public-transkribuslite-25175868-issue-@incoming.gitlab.com",
  repo: "https://gitlab.com/readcoop/webdev/public/transkribuslite",
  consentCookie: "cookieaccepted",
  recordSession: true,
};
