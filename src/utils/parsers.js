/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import * as yup from "yup";
import clone from "lodash/clone";
import { decode } from "~/utils/hashids.js";

export function parseSearchQuery(query) {
  if (typeof query.scope === "string") query.scope = [query.scope];

  const schema = yup.object().shape({
    term: yup.string().trim(),
    pagination: yup.number().integer().min(1),
    years: yup
      .array()
      .of(yup.number().integer().min(1800).max(1900))
      .min(2)
      .max(2)
      .nullable(),
    scope: yup.array().of(yup.string()).max(3).nullable(),
    sort: yup.string().oneOf(["year", "-year", "page", "-page", "relevance"]),
    view: yup.string().oneOf(["context", "thumbnail", "word"]),
  });

  try {
    return clone(schema.cast(query));
  } catch (err) {
    console.error(err);
    return {};
  }
}

export function parseDetailQuery(query) {
  const schema = yup.object().shape({
    id: yup.string().trim().required(),
    term: yup.string().trim(),
    result: yup.number().integer().min(0),
    search: yup.string(),
    view: yup.string().oneOf(["content", "image", "both"]).default("both"),
  });

  const data = schema.cast(query);

  const [documentId, pageNum, ...args] = decode(data.id);

  const otherSchema = yup.object().shape({
    documentId: yup.number().integer().required(),
    pageNum: yup.number().integer().required(),
  });

  otherSchema.validate({ documentId, pageNum });

  return {
    documentId,
    pageNum,
    term: data.term,
    search: data.search,
    view: data.view,
  };
}

export function parseBrowseQuery(query) {
  if (typeof query.scope === "string") query.scope = [query.scope];

  const schema = yup.object().shape({
    scope: yup.array().of(yup.string()).max(3),
  });

  return clone(schema.cast(query));
}
