/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

// const jp = require('jsonpath')
// const { morphism } = require('morphism')

import jsonpath from "jsonpath";
import morphism from "morphism";
import { contourDensity } from "d3";

export function transform(data, { isWrapped } = {}) {
  const results = jsonpath
    .apply(data, "$..PageHit[:]", (data) =>
      morphism(
        {
          id: {
            document: "docId",
            page: "pageNr",
          },
          meta: {
            title: "docTitle",
            page: "pageNr",
            collections: "collectionIds.collectionId",
          },
          image: {
            id: "pageUrl",
            width: 5409,
            height: 4594,
          },
          results: (data) => data.wordCoords.map(parseWordCoords),
          items: (data) => data.highlights.map(parseHighlight),
        },
        data
      )
    )
    .map(({ value }) => value);

  return {
    offset: data.start,
    limit: data.rows,
    total: data.numResults,
    items: results,
  };
}

function parseWordCoords(string) {
  const parts = string.split(":");
  const lineId = parts[1].split("/");
  let coords = parts[2].split(" ");

  const points = coords.map((point) => {
    const coord = {};
    let split = point.split(",");
    coord.x = split[0];
    coord.y = split[1];
    return coord;
  });

  let rect = toRect(points);
  const result = { id: lineId[2], label: parts[0], rect: rect };
  return result;
}

function toRect(points) {
  const xMin = Math.min(...points.map((p) => p.x));
  const xMax = Math.max(...points.map((p) => p.x));
  const yMin = Math.min(...points.map((p) => p.y));
  const yMax = Math.max(...points.map((p) => p.y));

  return {
    x: xMin,
    y: yMin,
    width: xMax - xMin,
    height: yMax - yMin,
  };
}

function parseHighlight(string) {
  const parts = string.split(/<em>|<\/em>/);
  const result = [];
  /* NOTE:
   *
   * When encountering '', ignore it, and flip wasMatch. First string
   * encountered should be considered non-match.
   *
   */
  let wasMatch = true;
  while (parts.length > 0) {
    const string = parts.shift();
    if (string !== "" && wasMatch) {
      result.push({ string });
    } else if (string !== "") {
      result.push({
        isMatch: true,
        string,
      });
    }
    wasMatch = !wasMatch;
  }
  return result;
}

// if (process.env.NODE_ENV !== 'production') {
//   Object.assign(transform, {
//     parseHighlight
//   })
// }

// module.exports = transform
