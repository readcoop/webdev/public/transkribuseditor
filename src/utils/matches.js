/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

function find(terms, data) {
  const mapping = [];
  let offset = 0;
  let content = "";

  for (const item of data) {
    const string = item.content.replace(/¬$/g, "");
    mapping.push({
      start: offset,
      length: string.length,
      element: item,
    });
    offset += string.length;
    content += string;
  }

  const highlights = [];

  for (const term of terms) {
    const index = content.toLowerCase().search(term.toLowerCase());

    for (const item of mapping) {
      const { start, length, element } = item;
      if (start <= index + term.length && start + length >= index) {
        highlights.push({
          id: item.element.id,
          term,
        });
      }
    }
  }

  return highlights;
}

function findMatches(text, terms) {
  // find matching segments
  const segments = [];

  for (const term of terms.map((t) => t.toLowerCase())) {
    let s = text.toLowerCase();
    let m = [];
    let index = 0;
    let offset = 0;
    do {
      index = s.indexOf(term);
      if (index !== -1) {
        offset += index;
        segments.push({
          start: offset,
          stop: offset + term.length,
        });
        s = s.slice(index + term.length);
        offset += term.length;
      }
    } while (index !== -1 && term.length < s.length);
  }

  // no matches found
  if (segments.length === 0) {
    return [{ string: text }];
  }

  // eliminate overlapping segments
  segments.sort((s, t) => s.start - t.start);
  const cleaned = [];

  let last = null;
  for (const s of segments) {
    if (last !== null) {
      // test for overlap, i.e. NOT non-overlap
      if (!(last.stop < s.start || s.stop < last.start)) {
        // elminate shorter segment
        if (last.stop - last.start < s.stop - s.start) {
          cleaned[cleaned.length - 1] = s;
        }
      }
    } else {
      cleaned.push(s);
      last = s;
    }
  }

  // resolve everything
  const parts = [];
  for (const s of cleaned) {
    if (s.start > 0) {
      parts.push({
        string: text.slice(0, s.start),
      });
    }
    parts.push({
      string: text.slice(s.start, s.stop),
      isMatch: true,
    });
  }

  if (cleaned.length > 0) {
    parts.push({
      string: text.slice(cleaned[cleaned.length - 1].stop),
    });
  }

  return parts;
}

function processSearchResults(results, content) {
  // deal with case where there is no page content
  if (content === null) {
    return [];
  }

  // get all matching strings from snippets
  const matches = results
    .reduce(
      // flatten, pick matching
      (arr, parts) => [
        ...arr,
        ...parts.filter((p) => p.isMatch).map((p) => p.string),
      ],
      []
    )
    .sort() // deduplicate
    .reduce((arr, s) => (arr.includes(s) ? arr : [...arr, s]), []);

  // mark matches in page content text
  return content.map((item) => {
    const segments = findMatches(item.content, matches);
    return {
      ...item,
      segments,
      highlight: segments.filter((item) => item.isMatch).length > 0,
    };
  });

  const x = content.reduce(
    (obj, i) =>
      Object.assign(
        {},
        {
          [i.id]: findMatches(i.content, matches.slice()),
        }
      ),
    {}
  );
  return results.map((result) => {
    const s = result.map((r) => r.string).join("");
    return findMatches(s, matches);
  });
}

export { findMatches, processSearchResults };

{
  // console.log(findMatches('this is some nice text', ['some', 'so', 'me']));
  // console.log(findMatches('this is some nice text', ['berta', 'somethinginteresting']));
}

{
  // const highlights = find(['Hoppbichter', 'Obermayer', 'Schererbauen'], require('./content.json'));
  // console.debug(highlights);
}
