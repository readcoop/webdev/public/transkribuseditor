/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

function parseCustomAttribute(string) {
  console.assert(typeof string === "string");

  if (string === "") return [];

  return string.split("} ").reduce(function (results, string) {
    var result = {};
    var attributes = {};

    var name = string.split(" {")[0];
    var attrs = string.split(" {")[1];

    if (attrs && attrs.indexOf("}")) attrs = attrs.replace("}", "");

    var key, value;

    return results.concat({
      name: name.trim(),
      attributes: attrs.split(";").reduce(function (attrs, string) {
        string = string.trim();
        if (string) {
          key = string.split(":")[0].trim();
          value = decodeURIComponent(
            JSON.parse(
              '"' + string.split(":")[1].trim().replace('"', '\\"') + '"'
            )
          );
          if (key === "index" || key === "offset" || key === "length")
            attrs[key] = parseInt(value);
          else if (
            key === "continued" ||
            key === "superscript" ||
            key === "bold" ||
            key === "italic" ||
            key === "subscript"
          )
            attrs[key] = Boolean(value);
          else attrs[key] = value;
        }
        return attrs;
      }, {}),
    });
  }, []);
}

function 
toCustomAttributeString(attrList) {
  //TODO here tag is set without attributes
  return attrList
    .reduce(function (string, item) {
      string += item.name + " {";
      for (var key in item.attributes) {
        var value;
        // item.attributes[key] contains correct value
        switch (typeof item.attributes[key]) {
          case "number":
          case "boolean":
            value = item.attributes[key].toString();
            break;
          case "string":
            // next line was missing as value not initalized was always set to null
            value = item.attributes[key].toString();
            value = value ? toUnicode(item.attributes[key]) : null;
            break;
          default:
            value = null;
            console.warn(
              "Dropping value",
              key,
              item.attributes[key],
              "for",
              item.name
            );
        }
        if (value !== null) {
          string += key + ":" + value + "; ";
        }
      }
      return string.trim() + "} ";
    }, "")
    .trim();
}

function toUnicode(string) {
  

  // return string.split('').map(function (value, index, array) {
	// 	var temp = value.charCodeAt(0).toString(16).toUpperCase();
	// 	if (temp.length > 2) {
	// 		return '\\u' + temp;
	// 	}
	// 	return value;
  // }).join('');
  
  // this function messes to attributes up
  var result = "";
  for (var i = 0; i < string.length; i++) {
    // NOTE: assumes all characters are < 0xffff
    if (/^[a-zA-Z0-9]*$/.test(string[i])) result += string[i];
    else
      result +=
        "\\u" + ("000" + string[i].charCodeAt(0).toString(16)).substr(-4);
  }
  return result;
}

function parseCustomAttributeFromNode(node) {
  var string = node.getAttribute("custom");

  if (string === null) return [];

  return parseCustomAttribute(string);
}

function parseReadingOrder(node) {
  return findReadingOrder(
    parseCustomAttribute(node.getAttribute("custom") || "")
  );
}

function findReadingOrder(attrList) {
  for (var index = 0; index < attrList.length; index++) {
    if (attrList[index].name === "readingOrder")
      return attrList[index].attributes["index"];
  }
  return null;
}

function hasTextLines(doc) {
  return doc.querySelector("TextLine") !== null;
}

function hasTextRegions(doc) {
  return doc.querySelector("TextRegion") !== null;
}

function hasEmptyLines(doc) {
  return doc.querySelector("TextEquiv > Unicode:empty") !== null;
}

var SELECTOR = {
  TextLine: "TextRegion > TextLine, TableRegion > TableCell > TextLine",
};

function hasReadingOrder(doc, selector) {
  selector = selector || SELECTOR.TextLine;

  var items = doc.querySelectorAll(selector);

  for (var index = 0; index < items.length; index++) {
    var first = parseCustomAttribute(items[index]).shift();
    if (first.name !== "readingOrder") return false;
  }

  return true;
}

function hasTags(doc) {
  var items = doc.querySelectorAll("TextLine[custom]");
  for (var i = 0; i < items.length; i++) {
    var attrList = parseCustomAttribute(items[i].getAttribute("custom"));
    for (var j = 0; j < attrList.length; j++) {
      var attrs = attrList[j].attributes || {};
      if ("offset" in attrs && "length" in attrs) return true;
    }
  }
  return false;
}

function hasTextLinesWithoutUnicode(doc) {
  var textLines = doc.querySelectorAll("TextLine > Unicode");
  var textLinesWithoutUnicode = doc.querySelectorAll(
    "TextLine > TextEquiv > Unicode"
  );
  return textLines.length === textLinesWithoutUnicode.length;
}

// NOTE: commented out because es6 syntax

// function textLineHasCoords (doc) {
//   return Array.prototype.map.call(doc.querySelectorAll('TextLine'), node => Array.prototype.map.call(node.childNodes, child => child.nodeName === 'Coords').reduce((acc, cur) => acc = acc || cur, false)).reduce(function (acc, cur) { return acc || cur; }, false);
// }

// function textLineHasCoordsWithPoints (doc) {
//   return Array.prototype.map.call(doc.querySelectorAll('TextLine'), node => Array.prototype.map.call(node.childNodes, child => child.nodeName === 'Coords' && child.getAttribute('points').length > 0).reduce((acc, cur) => acc = acc || cur, false)).reduce(function (acc, cur) { return acc || cur; }, false);
// }

// function textLineHasCoordsWithPointsAndFormat (doc) {
//   return Array.prototype.map.call(doc.querySelectorAll('TextLine'), node => Array.prototype.map.call(node.childNodes, child => child.nodeName === 'Coords' && /[(\d+,\d+\s\d+,\d+)\s]+/.test(child.getAttribute('points'))).reduce(function (acc, cur) { return acc || cur; }, false)).reduce(function (acc, cur) { return acc || cur; }, false);
// }

var SELECTOR = {
  TextLine: "TextRegion > TextLine, TableCell > TextLine",
};

function fixTextLinesWithoutUnicode(doc, selector) {
  selector =
    selector || "TextRegion > TextLine, TableRegion > TableCell > TextLine";

  var lines = doc.querySelectorAll(selector);
  var line;
  var equiv;
  var unicode;

  var namespace = doc.documentElement.getAttribute("xmlns");

  console.assert(namespace !== null && namespace !== "");

  for (var index = 0; index < lines.length; index++) {
    line = lines[index];
    equiv = line.querySelector("TextEquiv");

    if (equiv === null)
      equiv = line.appendChild(doc.createElementNS(namespace, "TextEquiv"));

    unicode = equiv.querySelector("Unicode");

    if (unicode === null) {
      unicode = equiv.appendChild(doc.createElementNS(namespace, "Unicode"));
      unicode.textContent = "";
    }
  }

  return doc;
}

function toRect(points) {
  var leftMost = Infinity;
  var rightMost = -Infinity;
  var topMost = Infinity;
  var bottomMost = -Infinity;

  points.forEach(function (point) {
    var x = point.x;
    var y = point.y;
    if (x < leftMost) leftMost = x;
    if (x > rightMost) rightMost = x;
    if (y > bottomMost) bottomMost = y;
    if (y < topMost) topMost = y;
  });

  return {
    x: leftMost,
    y: topMost,
    width: rightMost - leftMost,
    height: bottomMost - topMost,
  };
}

function calcAvg() {
  return (
    values.reduce(function (a, b) {
      return a + b;
    }, 0) / values.length
  );
}

function calcAvgDist(X) {
  var A; // baseline A, B
  var B;

  var avg = 0;

  for (var k = 0; k < X.length - 1; k++) {
    A = X[k];
    B = X[k + 1];

    var a = A[0]; // points on baseline
    var b = B[0];

    var d; // distance between baselines

    var nIter = 10000;

    for (var i = 0, j = 0; i < A.length && j < B.length; ) {
      if (nIter-- <= 0)
        throw new Error("Exceedeed maximum number of iterations!");

      /* consume points  */

      for (a = A[i]; a && a[0] <= b[0]; i++, a = A[i]) {
        d = Math.abs(b[1] - a[1]);
        avg += (d - avg) / (k + 1);
      }

      if (!a) a = A[A.length - 1];

      /* swap A and B rather than repeat loop ... */

      var T = A;
      A = B;
      B = T;

      var t = a;
      a = b;
      b = t;

      var l = i;
      i = j;
      j = l;
    }
  }

  return avg;
}

function parsePointsAttribute(string) {
  return string
    .split(" ")
    .map(function (string) {
      return string.split(",").map(Number);
    })
    .map(function (array) {
      return new Point(array[0], array[1]);
    });
}

var Point = (function () {
  Point.prototype.toArray = function toArray() {
    return [this.x, this.y];
  };

  Point.prototype.toString = function () {
    return this.x + "," + this.y;
  };

  return Point;

  function Point(x, y) {
    this.x = x;
    this.y = y;
  }
})();

function pointToString(p) {
  // cast coordinates to whole number as expert client get parse exception
  var xCord = Math.floor(p.x);
  var yCord = Math.floor(p.y);
  return xCord + "," + yCord;
}

function pointToArray(point) {
  return point.toArray();
}

var Point = (function () {
  Point.prototype.toArray = function toArray() {
    return [this.x, this.y];
  };

  Point.prototype.toString = function () {
    return this.x + "," + this.y;
  };

  return Point;

  function Point(x, y) {
    this.x = x;
    this.y = y;
  }
})();

function parsePointsAttribute(string) {
  return string
    .split(" ")
    .map(function (string) {
      return string.split(",").map(Number);
    })
    .map(function (array) {
      return new Point(array[0], array[1]);
    });
}

function calcLineHeight(doc) {
  var nodeList = Array.from(
    doc.querySelectorAll(
      "TextRegion > TextLine, TableRegion > TableCell > TextLine"
    )
  );

  var X = nodeList.map(function (node) {
    return parsePointsAttribute(
      node.querySelector(":scope > Coords").getAttribute("points")
    ).map(function (point) {
      return [point.x, point.y];
    });
  });

  return calcAvgDist(X);
}

function calcAvgLineHeight(points) {
  var avgHeight = 0;

  var height;

  var centerY = calcMean(
    points.map(function (p) {
      return p[1];
    })
  );

  var upperY;
  var lowerY;

  /* remove leftmost and rightmost points ... */
  var minX = Math.min.apply(
    Math,
    points.map(function (p) {
      return p[0];
    })
  );
  var maxX = Math.max.apply(
    Math,
    points.map(function (p) {
      return p[0];
    })
  );
  var width = maxX - minX;
  var alpha = 0.05;
  var leftMostX = minX + width * alpha;
  var rightMostX = maxX - width * alpha;

  for (var index = 0, y; index < points.length; index++) {
    p = points[index];

    x = p[0];

    if (x < leftMostX) continue;
    if (x > rightMostX) continue;

    y = p[1];

    if (y > centerY) {
      upperY = y;
    } else {
      lowerY = y;
    }

    if (upperY !== undefined && lowerY !== undefined) {
      height = upperY - lowerY;
      avgHeight += (height - avgHeight) / (index + 1);
    }
  }

  return avgHeight;
}

function Path(points) {
  /* FIXTHIS: inconsistent class pattern here ... */
  points = points || [];

  this.getPoints = function getPoints() {
    return points;
  };

  /* FIXTIHS: for performance, use cache decorator here */
  this.getRect = function getRect() {
    var leftMost = Infinity;
    var rightMost = -Infinity;
    var topMost = Infinity;
    var bottomMost = -Infinity;

    points.forEach(function (point) {
      var x = point.x;
      var y = point.y;

      if (x < leftMost) leftMost = x;
      if (x > rightMost) rightMost = x;
      if (y > bottomMost) bottomMost = y;
      if (y < topMost) topMost = y;
    });

    return {
      x: leftMost,
      y: topMost,
      width: rightMost - leftMost,
      height: bottomMost - topMost,
    };
  };
}

function findLeftMostPoint(points) {
  var p;
  var min = Infinity;
  var result;

  for (var index = 0, length = points.length; index < length; index++) {
    p = points[index];

    if (p.x < min) {
      result = p;
      min = p.x;
    }
  }
  return result;
}

function findConvexHull(S) {
  /* jarvis algorithm. not particularly efficient */

  var hullPoint = findLeftMostPoint(S);
  var candidatePoint;
  var point;

  var P = [];

  var slope;
  var intercept;

  var i = 0;

  function isLeft(a, b, c) {
    // https://stackoverflow.com/a/3461533
    return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x) > 0;
  }

  do {
    P[i] = hullPoint;
    candidatePoint = S[0];
    for (var j = 0, length = S.length; j < length; j++) {
      point = S[j];
      if (
        candidatePoint === hullPoint ||
        isLeft(hullPoint, candidatePoint, point)
      ) {
        candidatePoint = point;
      }
    }
    i++;
    hullPoint = candidatePoint;
  } while (P[0] !== candidatePoint);

  return P;
}

function buildPageContent(data) {
  var pathAttr = "coords";
  var baselineAttr = "baseline";

  /* FIXTHIS: add image retc somewhere ... */

  var state = new TaskState(children);
  data = data.lines || [];
  var path = new Path(
    findConvexHull(
      data
        .reduce(function (a, b) {
          return a.concat(b[pathAttr]);
        }, [])
        .map(function (p) {
          return new Point(p[0], p[1]);
        })
    )
  );

  var offset = path.getRect();

  offset.x *= -1;
  offset.y *= -1;

  var children = data.map(function (data) {
    function createPoints(points, offset) {
      offset = offset || { x: 0, y: 0 };
      return points.map(function (p) {
        /* NOTE: get relative to container */
        return new Point((offset.x || 0) + p[0], (offset.y || 0) + p[1]);
      });
    }

    var path = new Path(createPoints(data[pathAttr] || [], offset));
    var baseline = new Path(
      createPoints(data[baselineAttr] || [], {
        x: offset.x,
        /* NOTE set baseline offset */

        y: offset.y + 0.1 * path.getRect().height,
      })
    );

    var state = new State(false);
    var line = new Line(path, baseline, state);

    return {
      getBaseline: function () {
        return line._baseline.getPoints();
      },
      getRect: function () {
        return line._path.getRect();
      },
      getPath: function () {
        return line._path.getPoints();
      },
      // isDone: function () { return line.state.isDone(); }
    };
  });

  // var task = new Task(children, path, state);
  // task._path.getRect();

  return {
    getChildren: function () {
      return task._children;
    },
    isDone: function () {
      return task.state.isDone();
    },
    getRect: function () {
      return task._path.getRect();
    },
    getPath: function () {
      return task._path.getPoints();
    },
    getLineHeight: function () {
      return calcAvgDist(
        this.getChildren().map(function (line) {
          return line.getBaseline().map(function (p) {
            return [p.x, p.y];
          });
        })
      );
    },
    getFontSize: function () {
      var avg = 0;

      this.getChildren().forEach(function (line, index) {
        var pivot = calcAverage(
          line.getBaseline().map(function (p) {
            return p.y;
          })
        );
        var upperHull = line.getPath().filter(function (point) {
          return point.y <= pivot;
        });

        var m = calcAvgDist([
          line.getBaseline().map(pointToArray),
          upperHull.map(pointToArray),
        ]);

        avg += (m - avg) / (index + 1);
      });

      return avg;
    },
  };
}

/* reading order */

function OrderedTextLineIterator(doc) {
  // NOTE: doc must have reading order

  var SELECTOR = {
    REGION: "TextRegion, TableRegion",
    LINE: "TextRegion > TextLine, TableCell > TextLine",
  };

  var regionList = Array.from(doc.querySelectorAll(SELECTOR.REGION));

  var regionOrder = new ReadingOrderIndex(regionList);
  var regionNode;

  var lineList = [];
  var lineOrder;

  regionList.sort(regionOrder.compare);

  (function expandTableRegion() {
    for (var i = regionList.length - 1; i >= 0; i--) {
      if (regionList[i].nodeName === "TableRegion") {
        // replace region node with it's table cells
        var regionNode = regionList[i];

        var cellNodes = Array.from(regionNode.querySelectorAll("TableCell"));
        cellNodes.sort(function (a, b) {
          var d =
            parseInt(a.getAttribute("row")) - parseInt(b.getAttribute("row"));
          if (d !== 0) return d;
          return (
            parseInt(a.getAttribute("col")) - parseInt(b.getAttribute("col"))
          );
        });

        var args = [].concat.apply([i, 1], cellNodes);
        regionList.splice.apply(regionList, args);
      }
    }
  })();

  return {
    next: function () {
      var isDone = regionList.length === 0 && lineList.length === 0;

      // iterate
      while (lineList.length === 0 && regionList.length > 0) {
        regionNode = regionList.shift();
        lineList = Array.from(regionNode.querySelectorAll(SELECTOR.LINE));

        if (lineList.length > 0) {
          lineOrder = new ReadingOrderIndex(lineList);
          lineList.sort(lineOrder.compare);
        }
      }

      var item = lineList.shift();

      return {
        item: item,
        done: isDone,
      };
    },
  };
}

function getReadingOrder(node) {
  var string = node.getAttribute("custom");

  var none = {
    name: "readingOrder",
    attributes: { index: Number.MAX_SAFE_INTEGER },
  };

  var warning =
    "TextLine " + node.getAttribute("id") + ": " + "readingOrder not specified";

  if (string === null) {
    console.warn(warning);
    return none;
  }

  var attrList = parseCustomAttribute(string);
  var readingOrder = attrList.shift();

  if (readingOrder.name === "readingOrder") return readingOrder;

  for (var index = 0; index < attrList.length; index++) {
    readingOrder = attrList[index];
    if (readingOrder.name === "readingOrder") return readingOrder;
  }

  console.warn(warning);
  return none;
}

function ReadingOrderIndex(nodes) {
  var readingOrderIndex = [];
  var nodeList = [];

  for (var index = 0; index < nodes.length; index++) {
    var node = nodes[index];
    var readingOrder = getReadingOrder(node);

    console.assert(readingOrder.attributes instanceof Object);
    console.assert(typeof readingOrder.attributes.index === "number");

    nodeList.push(node);
    readingOrderIndex.push(readingOrder.attributes.index);
  }

  function get(node) {
    return readingOrderIndex[nodeList.indexOf(node)];
  }

  return {
    get: function (node) {
      return get(node);
    },
    compare: function (node, otherNode) {
      return get(node) - get(otherNode);
    },
  };
}

function traverseReadingOrder(node, visit, readingOrderedNodes) {
  // FIXME: how to deal with nodes that don't have reading order?

  visit(node);

  var nodes = Array.from(node.childNodes);

  if (
    readingOrderedNodes instanceof Array &&
    readingOrderedNodes.indexOf(node.nodeName) >= 0
  ) {
    var readingOrderIndex = buildReadingOrderIndex(nodes);
    nodes.sort(readingOrderIndex.compare);
  }

  for (var index = 0; index < nodes.length; index++) {
    traverseReadingOrder(nodes[index], visit);
  }
}

// *lol* of course we won't do this, just use XMLDocument.cloneNode(true);

function Node() {}

Node.prototype.getAttribute = function (name) {
  console.assert(name !== undefined);
  return this._attributes[name];
};

Node.prototype.setAttribute = function (name, value) {
  console.assert(name !== undefined);
  console.assert(value !== undefined);
  return (this._attributes[name] = value);
};

Node.prototype.getText = function () {
  return this._text;
};

Node.prototype.setText = function (text) {
  console.assert(typeof text === "string");
  return (this._text = text);
};

// function Page () {}

// Page.prototype = Object.create(Node);

// function TextRegion () {
// }

// TextRegion.prototype = Object.create(Node);

// function TextLine () {
// }

// TextLine.prototype.getText = function () {
//  return this._text;
// }

// TextLine.prototype.getAttributes = function () {
//   return this._attributes;
// }

// TextLine.prototype = Object.create(Node);

// function Unicode () {}

// Unicode.prototype = Object.create(Node);

// var Unicode = (function () {

//   Unicode.prototype.getText = function () {
//     return this._node.textContent;
//   };

//   Unicode.prototype.setText = function (text) {
//     // throw new Error("NotImplemented");
//     console.assert(typeof text === 'string');
//     return this._node.textContent = text;
//   };

//   return Unicode;

//   function Unicode (unicodeNode) {
//     this._node = unicodeNode;
//   }

// }());

// var Baseline = (function () {

//   Baseline.prototype.getPoints = function () {
//     return parsePointsAttribute(this._node.getAttribute('points'));
//   };

//   return Baseline;

//   function Baseline (baselineNode) {
//     this._node = baselineNode;
//   }

// }());

// var TextLine = (function () {

//   TextLine.prototype.getBaseline = function () {
//     return new Baseline(this._node.querySelector('Baseline'));
//   };

//   TextLine.prototype.getUnicode = function () {
//     return new Unicode(this._node.querySelector('TextEquiv > Unicode'));
//   };

//   return TextLine;

//   function TextLine (textLineNode) {
//     this._node = textLineNode;
//   }

// }());

// function PageContent (root) {

//   return {
//     getTextLines: function () {
//       return Array.from(root.querySelectorAll('Page > TextRegion > TextLine')).map(function (node) {
//         return new TextLine(node);
//       });
//     },
//     getTextRegions: function () {
//       return Array.from(root.querySelectorAll('Page > TextRegion'));
//     }
//   };
// }

// var Line = (function () {

//   function Line (path, baseline, state) {
//     this._path = path;
//     this._baseline = baseline;
//     this.state = state;
//   }

//   // Line.prototype.isDone = function isDone () {
//   //   return false;
//   // };

//   // Line.prototype.

//   return Line;

// }());

function PageXMLElementFactory(namespace) {
  // creates page xml objects ... elements and their attributes only
  return {
    createTextLine: createFactoryMethod("TextLine"),
    createTextRegion: createFactoryMethod("TextRegion"),
    createTextEquiv: createFactoryMethod("TextEquiv"),
    createUnicode: function (string) {
      var node = createElement("Unicode");

      if (string === null || string === undefined) string = "";

      node.textContent = string;
      return node;
    },
    createBaseline: function (points) {
      var node = createElement("Baseline");

      if (points.length > 0)
        node.setAttribute("points", points.map(pointToString).join(" "));

      return node;
    },
    createCoords: function (points) {
      var node = createElement("Coords");

      if (points.length > 0)
        node.setAttribute("points", points.map(pointToString).join(" "));

      return node;
    },
  };

  function createElement(name) {
    return document.createElementNS(namespace, name);
  }

  function createFactoryMethod(name) {
    return function () {
      return document.createElementNS(namespace, name);
    };
  }
}

/* helpers for writing changes to page xml documents */

function PageXMLSynch(doc) {
  var namespace = doc.documentElement.getAttribute("xmlns");

  console.assert(namespace !== null);

  var factory = new PageXMLElementFactory(namespace);

  function DoesNotExist() {
    this.name = "DoesNotExist";
    Error.apply(this, arguments);
  }
  DoesNotExist.prototype = Object.create(Error.prototype);
  DoesNotExist.constructor = DoesNotExist;

  function AssertionError() {
    this.name = "AssertionError";
    Error.apply(this, arguments);
  }
  AssertionError.prototype = Object.create(Error.prototype);
  AssertionError.constructor = AssertionError;

  return {
    updateTextLine: function (textLine) {
      var id = textLine.getId();

      var textEquiv = textLine.getTextEquiv();
      var unicode;

      if (textEquiv === null) return;
      else unicode = textEquiv.getUnicode();

      if (unicode === null) return;

      if (node === null) throw new DoesNotExist(id);

      var node = doc.getElementById(id);

      // var attrList = textLine.getCustomAttribute();
      // if (attrList.length > 0)
      //   node.setAttribute('custom', toCustomAttributeString(attrList));
      node.setAttribute("custom", textLine.getCustomAttribute());

      // var textEquivNode = node.querySelector('TextEquiv');
      var textEquivNode = findChildNode(node, "TextEquiv");

      if (textEquivNode === null) {
        textEquivNode = factory.createTextEquiv();
      } else if (textEquivNode.parentNode !== node) {
        throw new AssertionError();
      }

      var unicodeNode = textEquivNode.querySelector("Unicode");

      if (unicodeNode === null) {
        unicodeNode = factory.createUnicode();
      } else if (unicodeNode.parentNode !== textEquivNode) {
        throw new AssertionError();
      }

      unicodeNode.textContent = unicode.getText();

      function findChildNode(parent, name) {
        var items = parent.children;
        for (var index = 0; index < items.length; index++) {
          // NOTE: assumes that value is case sensitive
          if (items[index].nodeName === name) return items[index];
        }
        return null;
      }
    },

    addTextLine: function (textLine) {
      console.assert(textLine.parent !== null);

      var id = textLine.parent.getId();

      console.assert(id !== null);
      console.assert(doc.getElementById(id) === null);

      var parent = doc.getElementById(id);

      if (parent === null) throw new DoesNotExist(id);

      var textLineNode = factory.createTextLine();

      textLineNode.setAttribute("id", id);
      var attrList = textLine.getCustomAttribute();
      if (attrList.length > 0)
        textLineNode.setAttribute("custom", toCustomAttributeString(attrList));

      var textEquiv = textLine.getTextEquiv;
      if (textEquiv !== null) {
        var textEquivNode = factory.createTextEquiv();
        var unicode = textEquiv.getUnicode();
        if (unicode !== null) {
          var unicodeNode = factory.createUnicode(unicode.getText());
          textEquivNode.appendChild(unicodeNode);
        }
        textLineNode.appendChild(textEquivNode);
      }

      var baseline = textLine.getBaseline();
      if (baseline !== null) {
        var baselineNode = factory.createBaseline(baseline.getPoints());
        textLineNode.appendChild(baselineNode);
      }

      var coords = textLine.getCoords();
      if (coords !== null) {
        var coordsNode = factory.createCoords(coords.getPoints());
        textLineNode.appendChild(coordsNode);
      }

      parent.appendChild(textLineNode);
    },
    createTextLine: function (id, parent_id, coords, index) {
      var parent = doc.getElementById(parent_id);

      if (parent === null) throw new DoesNotExist(id);

      var textLineNode = factory.createTextLine();

      // Update all other reading order indices
      textLineNode.setAttribute("id", id);
      textLineNode.setAttribute(
        "custom",
        "readingOrder {index:" + index + ";}"
      );

      updateReadingOrder(parent, id, index);
      var textEquivNode = factory.createTextEquiv();
      var unicodeNode = factory.createUnicode();
      textEquivNode.appendChild(unicodeNode);
      textLineNode.appendChild(textEquivNode);

      var baselineNode = factory.createBaseline(coords);
      textLineNode.appendChild(baselineNode);

      var coordsNode = factory.createCoords(coords);
      textLineNode.appendChild(coordsNode);

      parent.appendChild(textLineNode);
    },

    addTextRegion: function (textRegion) {
      var id = textRegion.getId();

      console.assert(id !== null);
      console.assert(doc.getElementById(id) === null);

      var parentNode = doc.querySelector("Page");

      var textRegionNode = factory.createTextRegion();

      textRegionNode.setAttribute("id", id);

      var coords = textRegion.getCoords();
      if (coords !== null) {
        var coordsNode = factory.createCoords(coords.getPoints());
        textRegionNode.appendChild(coordsNode);
      }

      parentNode.appendChild(textRegionNode);
    },

    createTextRegion: function (id, coords) {
      var parentNode = doc.querySelector("Page");

      var textRegionNode = factory.createTextRegion();

      textRegionNode.setAttribute("id", id);

      if (coords !== null) {
        var coordsNode = factory.createCoords(coords);
        textRegionNode.appendChild(coordsNode);
      }

      parentNode.appendChild(textRegionNode);
    },

    deleteTextRegion: function (textRegion) {
      deleteNode(textRegion.getId());
    },

    deleteTextLine: function (textLine) {
      deleteNode(textLine.getId());
    },

    updateTextLineCoords: function (textLine, coords) {
      var baseline = textLine.getBaseline();
      // console.log(baseline);
      var id = textLine.getId();
      var textLineNode = doc.getElementById(id);

      if (baseline.getPoints() != null) {
        // create new Baseline node with factory
        var replaceNode = factory.createBaseline(coords);
        var coordsNode = textLineNode.querySelector("Baseline");
        textLineNode.replaceChild(replaceNode, coordsNode);
        // coordsNode.setAttribute('points', coords.map(pointToString));
      }
    },

    updateTextRegionCoords: function (textRegion, coords) {
      var coordsOld = textRegion.getCoords();
      var id = textRegion.getId();
      var textRegionNode = doc.getElementById(id);

      var parentNode = doc.querySelector("Page");

      if (coordsOld != null) {
        var oldcoordsNode = textRegionNode.querySelector("Coords");
        var coordsNode = factory.createCoords(coords);
        textRegionNode.replaceChild(coordsNode, oldcoordsNode);
      }
    },

    updateReadingTreeView: function (json) {
      for (var i = 0; i < json.length; i++) {
        var textRegion = doc.getElementById(json[i].id);
        var string = "readingOrder {index:" + i + ";}";
        if (textRegion !== null) {
          textRegion.setAttribute("custom", string);
        }

        var lines = json[i].children;

        for (var j = 0; j < lines.length; j++) {
          var textLine = doc.getElementById(lines[j].id);
          var textString = "readingOrder {index:" + j + ";}";
          // Check if textLine is null
          if (textLine !== null) {
            textLine.setAttribute("custom", textString);
          }
        }
      }
    },
  };

  function deleteNode(id) {
    var node = doc.getElementById(id);

    if (node === null) throw new DoesNotExist(id);

    node.parentNode.removeChild(node);
  }

  function updateReadingOrder(parent, id, index) {
    var textLines = parent.querySelectorAll("TextLine");

    for (var i = 0; i < textLines.length; i++) {
      var textLine = textLines[i];
      var readingIndex = getReadingOrder(textLine);
      if (readingIndex.attributes.index >= index) {
        var increased = readingIndex.attributes.index + 1;
        var string = "readingOrder {index:" + increased + ";}";
        textLine.setAttribute("custom", string);
      }
    }
  }
}

function updatePageXML(pageXML, pageModel) {
  // NOTE: deals with text line content only
  var synch = new PageXMLSynch(pageXML);

  var textLines = pageModel.getTextLines();
  var length = textLines.length;

  for (var index = 0; index < length; index++) {
    var textLine = pageModel.getTextLines()[index];
    synch.updateTextLine(textLine);
  }
}
