/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import config from "../config";
import { parsePageContent } from "./xml.js";
import pkg from "../../package.json";

const fetch = require("node-fetch");

const CONTENT = "https://transkribus.eu/r/zQPlDb1ZvVWJuiBU/api/v0";

async function logOut() {
  const response = await this.$auth.loginWith("keycloak");
}

export function getImageSize(imageUrl) {
  
}

export async function fetchSearchResults(
  { term, documents, titles, page, fuzziness },
  { offset = 0, limit = 10 }
) {
  const res = await window.$nuxt.$axios(
    "https://transkribus.eu/r/search/search",
    {
      method: "POST",
      body: JSON.stringify({
        term,
        collections: [collection],
        documents,
        titles,
        page,
        fuzziness,
        offset,
        limit,
      }),
      headers: {
        "Content-Type": CONTENT_TYPE,
        Accept: CONTENT_TYPE,
      },
    }
  );
  if (res.status === 200) return res.data;
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function fetchSearchFacets({
  term,
  documents,
  titles,
  fuzziness,
}) {
  const res = await window.$nuxt.$axios(
    "https://transkribus.eu/r/search/facets",
    {
      method: "POST",
      body: JSON.stringify({
        term,
        collections: [collection],
        documents,
        titles,
        fuzziness,
      }),
      headers: {
        "Content-Type": CONTENT_TYPE,
        Accept: CONTENT_TYPE,
      },
    }
  );
  const data = await res.json();
  if (res.status === 401) {
    logOut();
  }
  const { total, facets, params } = data;
  return {
    total,
    facets,
    params,
  };
}

export async function fetchSearchFulltext({ term, start, rows, filter }) {
  const res = await window.$nuxt.$axios.get("/search/fulltext", {
    params: {
      query: term,
      type: "LinesLc",
      start: start,
      rows: rows,
      filter: filter,
    },
  });
  if (res.status === 200) return res.data;
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function uploadImageJSON({ data, colId }) {
  const res = await window.$nuxt.$axios("/uploads", {
    method: "post",
    params: {
      collId: colId,
    },
    data: data,
  });
  if (res.status === 200) return res.data;
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function uploadImageData({ uploadId, formData }) {
  const res = await window.$nuxt.$axios("/uploads/" + uploadId, {
    method: "PUT",
    headers: {
      "Content-Type": "multipart/form-data",
    },
    data: formData,
    // onUploadProgress: e => {
    //   // updating progress indicator
    //   // progress(e.lengthComputable, e.loaded, e.total)
    // }
  });
  if (res.status === 200) return res.data;
  if (res.status === 401) return this.$auth.loginWith("keycloak");
  else throw new Error(res);
}

export async function uploadPDFData({ collId, formData }) {
  const res = await window.$nuxt.$axios(
    "/collections/" + collId + "/createDocFromPdf",
    {
      method: "POST",
      headers: {
        "Content-Type": "multipart/form-data",
      },
      data: formData,
    }
  );
  if (res.status === 200) return res.data;
  else throw new Error(res);
}

export async function fetchCollections({ offset, limit }) {
  const res = await window.$nuxt.$axios.get("/collections/list", {
    params: {
      nValues: limit,
      index: offset,
    },
  });
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function fetchCollectionsForUser({ userid }) {
  const res = await window.$nuxt.$axios.get(
    "/collections/" + userid + "/listCols",
    {}
  );
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function fetchUsersForCollection({ collId }) {
  const res = await window.$nuxt.$axios.get(
    "/collections/" + collId + "/userlist",
    {}
  );
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

// Get tag def from https://transkribus.eu/r/storage/collections/{colId}

export async function fetchStorageTagDef({ collId }) {
  const res = await window.$nuxt.$axios
    .get("https://transkribus.eu/r/storage/collections/" + collId)
    .catch(function (error) {
      console.log(error);
      throw new Error(res);
    });

  if (res.status === 200) {
    return res.data;
  }
}

export async function saveStorageTagDef({ data, collId }) {
  const res = await window.$nuxt.$axios.post(
    "https://transkribus.eu/r/storage/collections/",
    {
      data: data,
      id: collId,
    },
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  }
  // if (res.status === 401) {
  //   logOut()
  // } else throw new Error(res)
}

export async function fetchTagDef({ collId }) {
  const res = await window.$nuxt.$axios.get(
    "/collections/" + collId + "/listTagDefs",
    {}
  );
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function saveTagDef({ def, collId }) {

  const json = JSON.stringify(def);
  const res = await window.$nuxt.$axios.post(
    "/collections/" + collId + "/tagDefs",
    json,
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function findUser({ mail, firstName, lastName }) {
  const res = await window.$nuxt.$axios.get("/user/findUser", {
    params: {
      user: mail,
      firstName: firstName,
      lastName: lastName,
    },
  });
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function addModifyUserCollection({ collId, userId, role }) {
  const res = await window.$nuxt.$axios(
    "/collections/" + collId + "/addOrModifyUserInCollection",
    {
      method: "POST",
      params: {
        userid: userId,
        role: role,
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  } else if (res.status === 304) {
    throw new Error("304");
  } else throw new Error(res);
}

export async function removeUserCollection({ collId, userId }) {
  const res = await window.$nuxt.$axios(
    "/collections/" + collId + "/removeUserFromCollection",
    {
      method: "POST",
      params: {
        userid: userId,
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  } else if (res.status === 304) {
    throw new Error("304");
  } else throw new Error(res);
}

export async function deleteDocument({ collId, docId }) {
  const res = await window.$nuxt.$axios(
    "/collections/" + collId + "/" + docId,
    {
      method: "DELETE",
    }
  );
  if (res.status === 200) {
    return res.data;
  } else if (res.status === 304) {
    throw new Error("304");
  } else throw new Error(res);
}

export async function removeDocumentCollection({ collId, docId }) {
  const res = await window.$nuxt.$axios(
    "/collections/" + collId + "/removeDocFromCollection",
    {
      method: "POST",
      params: {
        id: docId,
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  } else if (res.status === 304) {
    throw new Error("304");
  } else throw new Error(res);
}

export async function addDocumentCollection({ collId, docId }) {
  const res = await window.$nuxt.$axios(
    "/collections/" + collId + "/addDocToCollection",
    {
      method: "POST",
      params: {
        id: docId,
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  } else if (res.status === 304) {
    throw new Error("304");
  } else throw new Error(res);
}

export async function fetchCollectionsMeta({ collId }) {
  const res = await window.$nuxt.$axios.get(
    "/collections/" + collId + "/metadata",
    {
      params: {
        stats: true,
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function fetchDocuments({ collId, offset, limit }) {
  const res = await window.$nuxt.$axios.get(
    "/collections/" + collId + "/list",
    {
      params: {
        nValues: limit,
        index: offset,
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function fetchDocumentsMeta({ collId, docId, limit }) {
  const res = await window.$nuxt.$axios.get(
    "/collections/" + collId + "/" + docId + "/metadata",
    {
      params: {
        nrOfTranscripts: limit,
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function fetchPages({ collId, docId }) {
  const res = await window.$nuxt.$axios.get(
    "/collections/" + collId + "/" + docId + "/fulldoc",
    {
      params: {
        nrOfTranscripts: 1,
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function fetchPagesString({ collId, docId, offset, limit }) {
  const res = await window.$nuxt.$axios.get(
    "/collections/" + collId + "/" + docId + "/pages",
    {
      params: {
        pages: offset + "-" + limit,
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function fetchPageContent({ id }) {
  const res = await window.$nuxt.$axios.get(
    config.searchApiBaseUrl + "/content/" + id,
    {
      method: "GET",
    }
  );
  if (res.status === 200) return res.data.PcGts.Page;
  if (res.status === 401) {
    logOut();
  }
  return null;
}

export async function fetchPageContentXML({ id }) {
  const res = await fetch(`https://files.transkribus.eu/Get?id=${id}`, {
    method: "GET",
  });
  const xml = await res.text();
  if (res.status === 200) {
    return parsePageContent(xml);
  }
  if (res.status === 401) {
    logOut();
  }
  return null;
}

export async function setPageContentXML({
  collId,
  id,
  page,
  xml,
  status,
  parentTranscriptId,
}) {
  const res = await window.$nuxt.$axios(
    `/collections/${collId}/${id}/${page}/text`,
    {
      method: "POST",
      headers: { "Content-Type": "text/xml" },
      params: {
        status: status,
        // parent: parentTranscriptId,
        toolName: "TranskribusLite" + pkg.version,
      },
      data: xml,
    }
  );
  if (res.status === 200) {
    return res.data;
  } else if (res.status === 304) {
    throw new Error("304");
  } else throw new Error(res);
}

export async function fetchContent(id) {
  const res = await fetch(`${CONTENT}/content/${id}`);
  if (res.status === 200) return res.json();
  return null;
}

export async function fetchImageMeta({ key }) {
  const res = await window.$nuxt.$axios.get(
    config.urls.image + "/" + key + "/info.json",
    {}
  );
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function fetchFacets({ ...params } = {}) {
  const res = await client.get("/facets", { params: params });
  if (res.status === 200) return res.data;
  if (res.status === 401) {
    logOut();
  }
  return null;
}

export async function fetchCounts({ type, ...params }) {
  if (!["scope", "year"].includes(type)) {
    console.error(`Unexpected type '${type}'`);
    return null;
  }

  const res = await client.get(`/counts/${type}`, { params: params });
  if (res.status === 200) return res.data;
  if (res.status === 401) {
    logOut();
  }
  return null;
}

export async function fetchItems({ path, offset, limit, ...others }) {
  const res = await client.get(`/items`, {
    params: {
      path,
      offset,
      limit,
      ...others,
    },
  });
  if (res.status !== 200) throw new Error(res.status);
  if (res.status === 401) {
    logOut();
  }
  return res.data;
}

export async function fetchScopeLabels({ scope, offset, limit, ...others }) {
  const res = await client.get(`/levels`, {
    params: {
      scope,
      offset,
      limit,
      ...others,
    },
  });
  if (res.status === 200)
    return res.data.map(({ value, count }) => ({
      label: value,
      count: count,
    }));
  if (res.status === 401) {
    logOut();
  }
  return null;
}

export async function fetchDocumentInfo({ id }) {
  const res = await window.$nuxt.$axios.get(`/documents/${id}`);
  if (res.status === 200) return res.data;
  if (res.status === 401) {
    logOut();
  }
  return null;
}

export async function fetchPageInfo({ collId, docId, pageNr }) {
  const res = await window.$nuxt.$axios.get(
    "/collections/" + collId + "/" + docId + "/" + pageNr,
    {}
  );
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function fetchPagesForDocument({ id, offset, limit }) {
  const res = await client.get(
    config.searchApiBaseUrl + `/documents/${id}/pages`,
    {
      params: {
        offset,
        limit,
      },
    }
  );
  if (res.status === 200) return res.data;
  if (res.status === 401) {
    logOut();
  }
  return null;
}

export async function getPublicModels() {
  const res = await window.$nuxt.$axios.get(
    "https://readcoop.eu/wp-json/wp/v2/model",
    {
      params: {
        per_page: 100,
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  }
}

export async function getHTRModels({ collId }) {
  const res = await window.$nuxt.$axios.get(`/recognition/${collId}/list`);
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function startHTR({ collId, htrModelId, docId, pageString }) {
  const res = await window.$nuxt.$axios(
    "/recognition/" + collId + "/" + htrModelId + "/htrCITlab",
    {
      method: "POST",
      params: {
        id: docId,
        pages: pageString,
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function startPylaia({ collId, modelId, docId, pageString }) {
  const res = await window.$nuxt.$axios(
    "/pylaia/" + collId + "/" + modelId + "/recognition",
    {
      method: "POST",
      params: {
        id: docId,
        pages: pageString,
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function createCollection({ collName }) {
  const res = await window.$nuxt.$axios("/collections/createCollection", {
    method: "POST",
    params: {
      collName: collName,
    },
  });
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function fetchJob({ jobId }) {
  const res = await window.$nuxt.$axios.get("/jobs/" + jobId, {
    params: {
      nrOfTranscripts: 1,
    },
  });
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function fetchCreditCost() {
  const res = await window.$nuxt.$axios.get("/credits/costs");
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function fetchCreditCalculation({ collId, modelId, pages }) {
  const res = await window.$nuxt.$axios(
    "/recognition/" + collId + "/" + modelId + "/costs",
    {
      method: "GET",
      params: {
        nrOfPages: pages,
      },
    }
  );
  if (res.status === 200) {
    return res.data;
  }
  // if (res.status === 401) {
  //   logOut()
  // } else throw new Error(res)
}

export async function fetchCredits() {
  const res = await window.$nuxt.$axios.get("/credits");
  if (res.status === 200) {
    return res.data;
  }
  // if(res.status === 401) {
  //   // logOut()
  //   var noCredits = {overallBalance: 0}
  //   return noCredits
  // }
  else throw new Error(res);
}

export async function fetchCreditsCollection({ collId }) {
  const res = await window.$nuxt.$axios.get(
    "/collections/" + collId + "/credits"
  );
  if (res.status === 200) {
    return res.data;
  } else throw new Error(res);
}

export async function fetchCreditsAssigned({ packageId }) {
  const res = await window.$nuxt.$axios.get(
    "/credits/" + packageId + "/collections"
  );
  if (res.status === 200) {
    return res.data;
  } else throw new Error(res);
}

export async function fetchCreditsTransactions({ packageId }) {
  const res = await window.$nuxt.$axios.get(
    "/credits/" + packageId + "/creditTransactions"
  );
  if (res.status === 200) {
    return res.data;
  } else throw new Error(res);
}

export async function addCreditsCollection({ collId, packageId }) {
  const res = await window.$nuxt.$axios(
    "/collections/" + collId + "/credits/" + packageId,
    {
      method: "POST",
    }
  );
  if (res.status === 200) {
    return res.data;
  } else if (res.status === 304) {
    throw new Error("304");
  } else throw new Error(res);
}

export async function removeCreditsCollection({ collId, packageId }) {
  const res = await window.$nuxt.$axios(
    "/collections/" + collId + "/credits/" + packageId,
    {
      method: "DELETE",
    }
  );
  if (res.status === 200) {
    return res.data;
  } else if (res.status === 304) {
    throw new Error("304");
  } else throw new Error(res);
}

export async function splitCredits({ packageId, balance }) {
  balance = parseFloat(balance);
  const res = await window.$nuxt.$axios("/credits/", {
    method: "POST",
    params: {
      sourcePackageId: packageId,
    },
    data: {
      balance,
    },
  });
  if (res.status === 200) {
    return res.data;
  } else if (res.status === 304) {
    throw new Error("304");
  } else throw new Error(res);
}

export async function fetchJobs({ offset, limit }) {
  const res = await window.$nuxt.$axios.get("/jobs/list", {
    params: {
      nValues: limit,
      index: offset,
      filterByUser: true,
    },
  });
  if (res.status === 200) {
    return res.data;
  }
  if (res.status === 401) {
    logOut();
  } else throw new Error(res);
}

export async function getXML({ url }) {
  const res = await window.$nuxt.$axios.get(url, {
    headers: {
      Accept: "application/xml",
    },
  });
  if (res.status === 200) return res.data;
  if (res.status === 401) {
    logOut();
  }
  return null;
}
