/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

export function prob2sigma(x) {
  if (x === 1.0) return x * 100;
  let z = 0;
  const lim = 4.53988e-5;
  const a = 0.2;
  z = x < lim ? lim : x > 1.0 ? 1.0 : x;
  let zz = 50.0 - (1.0 / a) * Math.log(1.0 / z - 1.0);
  if (zz < 0.000102575) zz = 0;
  if (zz > 100.0) zz = 100.0;
  return zz;
}

export function sigma2prob(zz) {
  const a = 0.2;
  const z = 1.0 / (Math.exp((50.0 - zz) * a) + 1);
  return z;
}

export function rgbhash(string) {
  // see https://gist.github.com/0x263b/2bdd90886c2036a1ad5bcf06d6e6fb37
  let hash = 0;
  if (string.length === 0) return hash;
  for (var i = 0; i < string.length; i++) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
    hash = hash & hash;
  }
  const rgb = [0, 0, 0];
  for (var i = 0; i < 3; i++) {
    const value = (hash >> (i * 8)) & 255;
    rgb[i] = value;
  }

  return rgb;
}

export function debounce(f, delay = 50) {
  let timeout = null;
  return function (...args) {
    if (timeout !== null) {
      clearTimeout(timeout);
    }
    timeout = setTimeout(() => f.apply(this, args), delay);
  };
}
