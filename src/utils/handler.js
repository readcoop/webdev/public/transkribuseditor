/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

const FIELDS = {
  ID: "id",
  DOCUMENT_ID: "docId",
  COLLECTION_ID: "collectionId",
  TITLE: "f_title",
  PAGE_NUM: "pageNr",
  SEARCH: "fullTextFromLinesLc",
  IMAGE_URL: "pageUrl",
  CONTENT_KEY: "xmlKey",
};

const URLS = {
  IMAGE: "https://files.transkribus.eu/iiif/2",
  CONTENT: "https://files.transkribus.eu/Get?id=",
};

function parseHighlight(string) {
  const parts = string.split(/<em>|<\/em>/);
  const result = [];
  /* NOTE:
   *
   * When encountering '', ignore it, and flip wasMatch. First string
   * encountered should be considered non-match.
   *
   */
  let wasMatch = true;
  while (parts.length > 0) {
    const string = parts.shift();
    if (string !== "" && wasMatch) {
      result.push({ string });
    } else if (string !== "") {
      result.push({
        isMatch: true,
        string,
      });
    }
    wasMatch = !wasMatch;
  }
  return result;
}

export function handleSearchResult(data) {
  const results = [];
  const snippets = [];

  for (const entry of data.docs) {
    const result = {
      id: entry[FIELDS.DOCUMENT_ID],
      title: entry[FIELDS.TITLE],
      page: entry[FIELDS.PAGE_NUM],
      snippets: [],
    };

    for (const snippet of data.highlighting[entry[FIELDS.ID]][
      FIELDS.SEARCH
    ].map(parseHighlight)) {
      result.snippets.push(snippet);
    }

    results.push(result);
  }

  return {
    total: data.response.numFound,
    // params: {
    //   ...params,
    //   offset: data.response.start
    // },
    results,
  };
}

// function handlePageSearchResult(data, params) {
//   const result = handleSearchResult(data, params);
//   const  { total, results, params, ...meta} = result;
//   return {
//     meta,
//     params,
//     results
//   };
// }

function toImageUrl(id) {
  return [URLS.IMAGE, id, "info.json"].join("/");
}

function toThumbUrl(id) {
  return `${URLS.IMAGE}/${id}/full/128,/0/default.jpg`;
}

function toContentUrl(id) {
  return `${URLS.CONTENT}${id}`;
}

function extractImageId(url) {
  const u = new URL(url);
  return u.searchParams.get("id");
}

function handleFacetResult(data, params) {
  const titleFacets =
    data.response.numFound > 0
      ? data.facet_counts.facet_fields[FIELDS.TITLE]
      : [];

  const counts = [];

  for (let index = 0; index < titleFacets.length; index += 2) {
    const [value, count] = titleFacets.slice(index, index + 2);
    counts.push({
      value: value,
      count: parseInt(count),
    });
  }

  return {
    total: data.response.numFound,
    params: {
      ...params,
      offset: data.response.start,
    },
    facets: {
      titles: counts,
    },
  };
}

function resolveCounts(counts) {
  return Object.keys(counts)
    .map((id) => parseInt(id))
    .map((id) => ({ id, count: counts[id] }))
    .sort((a, b) => b.count - a.count);
}

function handleDocumentsResult(data, params) {
  const groupData = data.grouped[FIELDS.DOCUMENT_ID];

  const items = groupData.groups
    .map((entry) => entry.doclist.docs[0])
    .map((data) => ({
      id: parseInt(data[FIELDS.DOCUMENT_ID]),
      title: data[FIELDS.TITLE],
    }));

  return {
    total: 0,
    params,
    items,
  };
}

function handleDocumentResult(data, params) {
  const firstEntry = data.grouped[FIELDS.DOCUMENT_ID].groups[0].doclist.docs[0];

  return {
    id: parseInt(firstEntry[FIELDS.DOCUMENT_ID]),
    title: firstEntry[FIELDS.TITLE],
    pageCount: data.grouped[FIELDS.DOCUMENT_ID].matches,
  };
}

function handlePagesResult(data, params) {
  const total = data.response.numFound;

  const items = data.response.docs.map((data) => ({
    id: data[FIELDS.DOCUMENT_ID],
    title: data[FIELDS.TITLE],
    number: data[FIELDS.PAGE_NUM],
    previewImage: toThumbUrl(extractImageId(data[FIELDS.IMAGE_URL])),
  }));

  return {
    total,
    params,
    items,
  };
}

function handleDetail(data, { document, pageNum }) {
  const { numFound, docs } = data.response;
  if (numFound === 0) return null;

  const pageData = docs[0];

  const image = toImageUrl(extractImageId(pageData[FIELDS.IMAGE_URL]));
  const content = toContentUrl(pageData[FIELDS.CONTENT_KEY]);

  return {
    image,
    content,
  };
}
