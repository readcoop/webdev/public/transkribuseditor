/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

const MAPPINGS = {
  "search-form": {
    term: "t",
    titles: "x",
    fuzziness: "f",
    page: "p",
  },
  paginated: {
    page: "p",
  },
  browser: {
    display: "d",
    textVisible: "v",
    term: "t",
    fuzziness: "f",
  },
};

function createMapperFromMapping(mapping) {
  const reverseMapping = Object.keys(mapping).reduce(
    (obj, key) => Object.assign(obj, { [mapping[key]]: key }),
    {}
  );
  return {
    toURL(source, { ignore = [] } = {}) {
      if (typeof source === "string") {
        return mapping[source];
      }

      const target = {};
      for (const key in source) {
        if (!ignore.includes(key)) {
          target[mapping[key]] = source[key];
        }
      }
      return target;
    },
    fromURL(source) {
      if (typeof source === "string") {
        return reverseMapping[source];
      }

      const target = {};
      for (const key in source) {
        target[reverseMapping[key]] = source[key];
      }
      return target;
    },
  };
}

export function createWatchers(
  target,
  handler,
  { ignore = [], immediate = true, deep = false } = {}
) {
  const watchers = {};
  const mapping = MAPPINGS["search-form"];
  const keys = Object.keys(mapping).filter((key) => !ignore.includes(key));
  for (const key of keys) {
    const value = mapping[key];
    console.assert(value !== undefined, "unexpected value undefined");
    watchers[target + "." + value] = {
      handler:
        typeof handler === "function"
          ? function (...args) {
              handler.call(this, ...args);
            }
          : handler,
      immediate,
      deep,
    };
  }
  return watchers;
}

export function createMapper(mappingName) {
  if (!(mappingName in MAPPINGS)) {
    throw new Error(`'${mappingName}' not in mappings`);
  }
  const mapping = MAPPINGS[mappingName];
  return createMapperFromMapping(mapping);
}
