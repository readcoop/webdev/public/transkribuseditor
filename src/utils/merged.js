/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

function* bigrams(items) {
  const [firstItem, ...otherItems] = items;
  const bigram = [firstItem];
  for (const item of otherItems) {
    bigram.push(item);
    if (bigram.length > 2) bigram.shift();
    yield bigram.slice();
  }
}

function merged(self, other, results, T) {
  if (other.rect.y - (self.rect.y + self.rect.height) < T) {
    const last = results[results.length - 1];
    results[results.length - 1] = {
      rect: {
        ...last.rect,
        height: other.rect.y + other.rect.height - last.rect.y,
      },
      items: [...last.items, other.item],
    };
  } else {
    results.push({
      rect: other.rect,
      items: [other.item],
    });
  }
  return results;
}

export default function (results, { maxY, padding, threshold }) {
  // const logged = (data, label) => console.log(data, label || '') || data
  // console.debug(
  //   'merged',
  //   merged(
  //     { rect: { y: 0, height: 10 }, item: 1 },
  //     { rect: { y: 5, height: 15 }, item: 2 },
  //     [{ rect: { y: 0, height: 10 }, items: [1] }]
  //   )
  // )

  // console.debug(
  //   'merged',
  //   merged(
  //     { y1: 0, y2: 10, item: 1 },
  //     { y1: 30, y1: 30, item: 2 },
  //     []
  //   )
  // )

  // console.debug(
  //   'merged',
  //   merged(
  //     { rect: { y: 0, height: -Infinity } },
  //     { rect: { y: 50, height: 100 }, item: 2 },
  //     []
  //   )
  // )

  const sortedResults = results
    .slice()
    .sort((self, other) => self.rect.y - other.rect.y);

  const padded = (item, padding) => {
    const { y, height } = item.rect;
    const yMid = y + height / 2;
    const y1 = Math.max(0, yMid - padding);
    const y2 = Math.min(maxY, yMid + padding);
    return {
      y: y1,
      height: y2 - y1,
    };
  };
  const croppedResults = sortedResults.map((item) => ({
    rect: padded(item, padding),
    item,
  }));

  const dummy = { rect: { y: 0, height: -Infinity } };
  return [...bigrams([dummy, ...croppedResults])].reduce(
    (results, [self, other]) => merged(self, other, results, threshold),
    []
  );

  // const [firstResult, ...otherCroppedResults] = croppedResults
  // const lastMergedResult = {
  //   rect: firstResult.rect,
  //   items: [firstResult.item]
  // }
  // const mergedResults = [lastMergedResult]

  // let lastEntry = firstResult
  // for (const entry of otherCroppedResults) {
  //   if (entry.rect.y - (lastEntry.rect.y + lastEntry.rect.height) < threshold) {
  //     // overlap
  //     const mergedEntry = mergedResults[mergedResults.length - 1]
  //     mergedEntry.items.push(entry.item)
  //     mergedEntry.rect = {
  //       ...mergedEntry.rect,
  //       height: entry.rect.y + entry.rect.height - mergedEntry.rect.y
  //     }
  //   } else {
  //     // no-overlap
  //     mergedResults.push({
  //       rect: entry.rect,
  //       items: [entry.item]
  //     })
  //   }
  //   lastEntry = entry
  // }

  // return mergedResults
}
