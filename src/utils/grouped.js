/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

export default function grouped(results) {
  const groups = new Map();
  for (const result of results) {
    if (groups.size === 0) groups.set(result.rect.y, [result]);
    else {
      let min = { dist: Infinity, pos: null };
      const thresh = 100;

      for (const [pos, entries] of groups) {
        const dist = Math.abs(pos - result.rect.y);
        if (dist < min.dist) min = { dist, pos };
      }

      if (min.dist < thresh) groups.get(min.pos).push(result);
      else groups.set(result.rect.y, [result]);
    }
  }
  return [...groups.keys()].sort().map((key) => groups.get(key));
}
