/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import "~/utils/PageUtils.js";

function PageModelFactory() {
  Node.prototype = Object.create(Node.prototype);
  Node.prototype.constructor = Node;
  Node.prototype.getName = function () {
    return Object.getPrototypeOf(this).constructor.name;
  };
  Node.prototype.isRoot = function () {
    return false;
  };
  Node.prototype.getChildren = function () {
    return this.children.slice();
  };
  Node.prototype.appendChild = function (node) {
    node.setParent(this);
    this.children.push(node);
  };
  Node.prototype.childAt = function (index) {
    if (index < 0 || index >= this.children.length) return null;
    return this.children[index];
  };
  Node.prototype.getData = function () {
    return this.data;
  };
  Node.prototype.setParent = function (node) {
    this.parent = node;
  };
  Node.prototype.getParent = function () {
    return this.parent;
  };
  Node.prototype.isLeaf = function () {
    return false;
  };
  function Node(data, parent) {
    this.data = data === undefined ? null : data;
    this.parent = parent === undefined ? null : parent;
    this.children = [];
  }

  Root.prototype = Object.create(Node.prototype);
  Root.prototype.constructor = Root;
  Root.prototype.setParent = function () {
    this.parent = null;
  };
  Root.prototype.isRoot = function () {
    return true;
  };
  function Root() {
    Node.call(this);
  }

  Leaf.prototype = Object.create(Node.prototype);
  Leaf.prototype.constructor = Leaf;
  Leaf.prototype.isLeaf = function () {
    return true;
  };
  Leaf.prototype.getChildren = function () {
    return [];
  };
  Leaf.prototype.appendChild = function () {};

  function Leaf() {
    this.children = [];
  }

  Page.prototype = Object.create(Root.prototype);
  Page.prototype.constructor = Page;
  // TODO: check if these methods are really needed ...
  Page.prototype.getTextRegions = function () {
    return this.getChildren()
      .filter(function (object) {
        return object instanceof TextRegion || object instanceof TableRegion;
      })
      .sort(sortByReadingOrder);
  };
  // NOTE: this one is required by text styling and annotation component in branch feature-tagging
  Page.prototype.getTextLines = function () {
    return this.getTextRegions().reduce(function (textLines, textRegion) {
      return textLines.concat(textRegion.getTextLines());
    }, []);
  };
  Page.prototype.isEmpty = function () {
    return this.getTextLines().every(function (t) {
      return t.getText() === "";
    });
  };
  function Page() {
    Node.call(this);
  }

  // TODO: use base / abstract class for code shared between TextRegion / TextLine

  TextRegion.prototype = Object.create(Node.prototype);
  TextRegion.prototype.constructor = TextRegion;
  TextRegion.prototype.getId = function () {
    return this.id;
  };
  TextRegion.prototype.setId = function (id) {
    this.id = id;
  };
  TextRegion.prototype.getTextLines = function () {
    return this.getChildren()
      .filter(function (object) {
        return object instanceof TextLine;
      })
      .sort(sortByReadingOrder);
  };
  TextRegion.prototype.getCoords = function () {
    return this.coords;
  };
  TextRegion.prototype.setCoords = function (coords) {
    this.coords = coords;
  };
  TextRegion.prototype.getReadingOrderIndex = function () {
    return this.readingOrderIndex;
  };
  function TextRegion(id, readingOrderIndex) {
    Node.call(this);
    this.id = id;
    this.coords = null;
    this.readingOrderIndex = readingOrderIndex;
  }

  TableRegion.prototype = Object.create(Node.prototype);
  TableRegion.prototype.constructor = TableRegion;
  TableRegion.prototype.getId = function (coords) {
    return this.id;
  };
  TableRegion.prototype.setCoords = function (coords) {
    this.coords = coords;
  };
  TableRegion.prototype.getTableCells = function () {
    return this.getChildren()
      .filter(function (object) {
        return object instanceof TableCell;
      })
      .sort(sortRowMajor);
  };
  TableRegion.prototype.getReadingOrderIndex = function () {
    return this.readingOrderIndex;
  };
  TableRegion.prototype.getTextLines = function () {
    return this.getTableCells()
      .reduce(function (textLines, tableCell) {
        return textLines.concat(tableCell.getTextLines());
      }, [])
      .filter(function (object) {
        return object instanceof TextLine;
      });
  };
  function TableRegion(id, readingOrderIndex) {
    Node.call(this);
    this.id = id;
    this.coords = null;
    this.readingOrderIndex = readingOrderIndex;
  }

  TableCell.prototype = Object.create(Node.prototype);
  TableCell.prototype.constructor = TableCell;
  TableCell.prototype.getId = function (coords) {
    return this.id;
  };
  TableCell.prototype.getReadingOrderIndex = function () {
    // NOTE: get reading order from parent
    return this.parent.getReadingOrderIndex();
  };
  TableCell.prototype.getRow = function () {
    return this.row;
  };
  TableCell.prototype.getColumn = function () {
    return this.column;
  };
  TableCell.prototype.getRowSpan = function () {
    return this.rowSpan;
  };
  TableCell.prototype.getColSpan = function () {
    return this.colSpan;
  };
  TableCell.prototype.setCoords = function (coords) {
    this.coords = coords;
  };
  TableCell.prototype.getTextLines = function () {
    return this.getChildren().sort(sortByReadingOrder);
  };
  function TableCell(id, row, column, rowSpan, colSpan) {
    Node.call(this);
    this.id = id;
    this.coords = null;
    this.row = row;
    this.column = column;
    this.rowSpan = rowSpan;
    this.colSpan = colSpan;
  }

  TextLine.prototype = Object.create(Node.prototype);
  TextLine.prototype.constructor = TextLine;
  TextLine.prototype.getReadingOrderIndex = function () {
    return this.readingOrderIndex;
  };
  TextLine.prototype.getId = function () {
    return this.id;
  };
  TextLine.prototype.setId = function (id) {
    this.id = id;
  };
  TextLine.prototype.getText = function () {
    return this.textEquiv.getText();
  };
  TextLine.prototype.setTextEquiv = function (textEquiv) {
    this.textEquiv = textEquiv;
  };
  TextLine.prototype.getTextEquiv = function () {
    return this.textEquiv;
  };
  TextLine.prototype.setBaseline = function (baseline) {
    this.baseline = baseline;
  };
  TextLine.prototype.getBaseline = function () {
    return this.baseline;
  };
  // TODO: use parseCustomAttribute and toCustomAttributeString here?
  TextLine.prototype.setCustomAttribute = function (customAttribute) {
    this.customAttribute = customAttribute;
  };
  TextLine.prototype.getCustomAttribute = function () {
    return this.customAttribute;
  };
  TextLine.prototype.getWords = function () {
    return this.getChildren().sort(sortByReadingOrder);
  };
  TextLine.prototype.getCoords = function () {
    return this.coords;
  };
  TextLine.prototype.setCoords = function (coords) {
    this.coords = coords;
  };
  function TextLine(id, readingOrderIndex, customAttribute) {
    Node.call(this);
    this.id = id;
    this.readingOrderIndex = readingOrderIndex;
    this.customAttribute = customAttribute || [];
    this.textEquiv = null;
  }

  // TODO: use abstract base class or mixin for text here ...
  Word.prototype = Object.create(Leaf.prototype);
  Word.prototype.constructor = Word;
  Word.prototype.setTextEquiv = function (textEquiv) {
    this.textEquiv = textEquiv;
  };
  function Word(readingOrderIndex) {
    Leaf.call(this);
    this.readingOrderIndex = readingOrderIndex;
  }

  Baseline.prototype = Object.create(Leaf.prototype);
  Baseline.prototype.constructor = Baseline;
  Baseline.prototype.getPoints = function () {
    return this.points;
  };
  function Baseline(points) {
    Leaf.call(this);
    this.points = points;
  }

  Coords.prototype = Object.create(Leaf.prototype);
  Coords.prototype.constructor = Coords;
  Coords.prototype.getPoints = function () {
    return this.points.slice();
  };
  function Coords(points) {
    Leaf.call(this);
    this.points = points;
  }

  TextEquiv.prototype = Object.create(Leaf.prototype);
  TextEquiv.prototype.constructor = TextEquiv;
  TextEquiv.prototype.setUnicode = function (unicode) {
    this.unicode = unicode;
  };
  TextEquiv.prototype.getUnicode = function () {
    return this.unicode;
  };
  TextEquiv.prototype.getText = function () {
    return this.unicode.getText();
  };
  function TextEquiv() {
    Leaf.call(this);
    this.unicode = null;
  }

  Unicode.prototype = Object.create(Leaf.prototype);
  Unicode.prototype.constructor = Unicode;
  Unicode.prototype.getText = function () {
    return this.string;
  };
  Unicode.prototype.setText = function (string) {
    this.string = string;
  };
  function Unicode(string) {
    Leaf.call(this);
    this.string = string;
  }

  function sortByReadingOrder(item, otherItem) {
    return item.getReadingOrderIndex() - otherItem.getReadingOrderIndex();
  }

  function sortRowMajor(item, otherItem) {
    var d = item.getRow() - otherItem.getRow();
    if (d !== 0) return d;
    return item.getColumn() - otherItem.getColumn();
  }

  function sortColumnMajor(item, otherItem) {
    var d = item.getColumn() - otherItem.getColumn();
    if (d !== 0) return d;
    return item.getRow() - otherItem.getRow();
  }

  var factoryMethods = {
    Root: function () {
      return new Root();
    },
    Page: function (node) {
      return new Page();
    },
    TextRegion: function (node) {
      return new TextRegion(node.getAttribute("id"), parseReadingOrder(node));
    },
    TableRegion: function (node) {
      return new TableRegion(node.getAttribute("id"), parseReadingOrder(node));
    },
    TableCell: function (node) {
      return new TableCell(
        node.getAttribute("id"),
        parseInt(node.getAttribute("row")),
        parseInt(node.getAttribute("col")),
        parseInt(node.getAttribute("rowSpan")),
        parseInt(node.getAttribute("colSpan"))
      );
    },
    TextLine: function (node, object) {
      return new TextLine(
        node.getAttribute("id"),
        parseReadingOrder(node),
        parseCustomAttribute(node.getAttribute("custom") || "")
      );
    },
    Word: function (node) {
      return new Word(parseReadingOrder(node));
    },
    Coords: function (node) {
      return new Coords(parsePointsAttribute(node.getAttribute("points")));
    },
    Baseline: function (node) {
      return new Baseline(
        parsePointsAttribute(node.getAttribute("points") || "")
      );
    },
    TextEquiv: function (node) {
      return new TextEquiv();
    },
    Unicode: function (node) {
      return new Unicode(!node ? "" : node.textContent);
    },
  };

  var reduceMethods = {
    Root: {
      Page: function (root, page) {
        root.appendChild(page);
      },
    },
    Page: {
      TextRegion: function (page, textRegion) {
        page.appendChild(textRegion);
      },
      TableRegion: function (page, tableRegion) {
        page.appendChild(tableRegion);
      },
    },
    TextRegion: {
      TextLine: function (textRegion, textLine) {
        textRegion.appendChild(textLine);
      },
      Coords: function (textRegion, coords) {
        textRegion.setCoords(coords);
      },
    },
    TableRegion: {
      TableCell: function (tableRegion, tableCell) {
        tableRegion.appendChild(tableCell);
      },
      Coords: function (tableRegion, coords) {
        tableRegion.setCoords(coords);
      },
    },
    TableCell: {
      TextLine: function (tableCell, textLine) {
        tableCell.appendChild(textLine);
      },
      Coords: function (tableCell, coords) {
        tableCell.setCoords(coords);
      },
    },
    TextLine: {
      Baseline: function (textLine, baseline) {
        textLine.setBaseline(baseline);
      },
      TextEquiv: function (textLine, textEquiv) {
        textLine.setTextEquiv(textEquiv);
      },
      Word: function (textLine, word) {
        textLine.appendChild(word);
      },
    },
    TextEquiv: {
      Unicode: function (textEquiv, unicode) {
        textEquiv.setUnicode(unicode);
      },
    },
    Word: {
      TextEquiv: function (word, textEquiv) {
        word.setTextEquiv(textEquiv);
      },
    },
  };

  var amendMethods = {
    TextLine: {
      test: function (textLine) {
        return textLine.getTextEquiv() instanceof TextEquiv;
      },
      apply: function (textLine) {
        var textEquiv;
        amend(
          (textEquiv = reduce(textLine, create("TextEquiv"))).getName(),
          textEquiv
        );
      },
    },
    TextEquiv: {
      test: function (textEquiv) {
        return textEquiv.getUnicode() instanceof Unicode;
      },
      apply: function (textEquiv) {
        reduce(textEquiv, create("Unicode"));
      },
    },
  };

  function create(name, node) {
    var f = factoryMethods[name];
    if (f === undefined) throw new NotImplementedError();
    return f(node);
  }

  function reduce(parent, object) {
    var m = reduceMethods[parent.getName()];
    if (m !== undefined) {
      var f = m[object.getName()];
      if (f !== undefined) {
        f(parent, object);
        return object;
      }
    }
  }

  function amend(name, object) {
    var amend = amendMethods[name];
    if (amend !== undefined && !amend.test(object)) {
      amend.apply(object);
    }
  }

  return {
    contains: function (name) {
      var r = name in factoryMethods;
      // if (r === false)
      //   console.warn("Not implemented:", name);
      return r;
    },
    reduce: function (parent, object) {
      reduce(parent, object);
    },
    amend: function (name, object) {
      amend(name, object);
    },
    create: function (name, node) {
      return create(name, node);
    },
  };
}

var PageXML = {
  factory: new PageModelFactory(),
  create: function (doc) {
    function build(node, O, factory) {
      O = factory.contains(node.nodeName)
        ? factory.create(node.nodeName, node)
        : O;

      for (var index = 0; index < node.children.length; index++) {
        factory.reduce(O, build(node.children[index], O, factory));
      }

      factory.amend(O.getName(), O);

      return O;
    }

    return build(doc.querySelector("Page"), null, this.factory);
  },
};

export function createModel(doc) {
  function build(node, O) {
    O = factory.contains(node.nodeName)
      ? factory.create(node.nodeName, node)
      : O;

    for (var index = 0; index < node.children.length; index++) {
      factory.reduce(O, build(node.children[index], O));
    }

    return O;
  }

  var factory = new PageModelFactory();
  return build(doc.querySelector("Page"));
}

function iterateBreadthFirst(node, object, create, reduce) {
  /* breadth-first iterator */

  var queue = [[node, object]];

  var render;

  while (queue.length > 0) {
    var item = queue.shift();

    var node = item[0];
    var object = item[1];

    object = reduce(object, create(node));

    if (object !== null)
      for (var index = 0; index < node.children.length; index++) {
        queue.push([node.children[index], object]);
      }
  }
}
