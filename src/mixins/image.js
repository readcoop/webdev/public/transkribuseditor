/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import config from "../config";
import round from "lodash/round";

export default {
  data() {
    return { imageBaseUrl: config.urls.image };
  },
  methods: {
    getPaddedRect({ x, y, width, height }, padding = 5) {
      return {
        x: x - padding,
        y: y - padding,
        width: width + 2 * padding,
        height: height + 2 * padding,
      };
    },
    getImageInfo(id) {
      return `${this.imageBaseUrl}/${id}/info.json`;
    },
    getImageSource(id, options = {}) {
      const { x = 0, y = 0, width = "", height = "" } = options;
      if (x === 0 && y === 0 && width === "" && height === "") {
        return `${this.imageBaseUrl}/${id}/full/full/0/default.jpg`;
      }
      return `${this.imageBaseUrl}/${id}/${x},${y},${width},${height}/full/0/default.jpg`;
    },
    getScaledSource(id, { width = "", height = "" }) {
      return `${this.imageBaseUrl}/${id}/full/${width},${height}/0/default.jpg`;
    },
    getThumbnailSource(id, params) {
      return this.getScaledSource(id, params);
    },
    getContextImageSource(id, { x, y, width, height }, toRect) {
      return `${this.imageBaseUrl}/${id}/${x},${y},${width},${height}/${toRect.width},${toRect.height}/0/default.jpg`;
    },
    getRoundedRect(rect, n = 2) {
      return {
        x: round(rect.x, 2),
        y: round(rect.y, 2),
        width: round(rect.width, 2),
        height: round(rect.height, 2),
      };
    },
    getIntersectionRatio(r1, r2) {
      // https://stackoverflow.com/a/27162334
      const dx =
        Math.min(r1.x + r1.width, r2.x + r2.width) - Math.max(r1.x, r2.x);
      const dy =
        Math.min(r1.y + r1.height, r2.y + r2.height) - Math.max(r1.y, r2.y);
      return Math.max(0, dx) * Math.max(0, dy);
    },
    getScaledRect({ x, y, width, height }) {
      const rounded = (x) => Math.round(x * 1000000) / 1000000;
      const w = this.width;
      return {
        x: rounded(x / w),
        y: rounded(y / w),
        width: rounded(width / w),
        height: rounded(height / w),
      };
    },
    getScaledPoints(points, width) {
    
      const rounded = (x) => Math.round(x * 1000) / 1000;
      const w = width;
      return points.map(({ x, y }) => ({
        x: rounded(x / w),
        y: rounded(y / w),
      }));
    },
  
    getPaddedRect({ x, y, width, height }, padding = 5) {
      return {
        x: x - padding,
        y: y - padding,
        width: width + 2 * padding,
        height: height + 2 * padding,
      };
    },
    parseNumber(value) {
      if (typeof value === "number") return value;
      if (typeof value === "string" && /\d+/.test(value))
        return parseInt(value);
      return null;
    },
    roundNumber(value, n) {
      return round(value, n);
    },
  },
};
