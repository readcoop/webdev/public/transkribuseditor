/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import omit from "lodash/omit";

export default {
  props: {
    name: {
      type: String,
      required: true,
    },
  },
  watch: {
    "$route.query": {
      handler(query) {
        if (this.values.includes(query[this.name])) {
          this.value = query[this.name];
        } else {
          this.value = this.defaultValue;
        }
      },
      immediate: true,
    },
    value(value) {
      if (!this.validate(value)) {
        this.$router.push({
          query: omit(this.$route.query, this.name),
        });
      } else {
        // drop if default
        if (value === this.defaultValue) {
          this.$router.push({
            query: omit(this.$route.query, this.name),
          });
        } else {
          this.$router.push({
            query: {
              ...this.$route.query,
              [this.name]: this.value,
            },
          });
        }
      }
    },
  },
  data() {
    return {
      value: this.defaultValue,
    };
  },
  computed: {
    values() {
      return this.options.map((option) => option.value);
    },
  },
  methods: {
    validate(value) {
      // NOTE: strict comparison
      return this.values.includes(value);
    },
  },
};
