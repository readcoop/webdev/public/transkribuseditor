/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import Highlight from "@tiptap/extension-highlight";

import { Node, mergeAttributes } from "@tiptap/core";

export async function tagExtensions(tagDefs) {
  var tagExtensionArr = [];
  for (var tagIndex in tagDefs) {
    var tag = tagDefs[tagIndex];
    var tempExtension = Highlight.extend({
      name: tag.name,
      addKeyboardShortcuts() {
        return {
          "Mod-l": () => this.editor.commands.toggleBulletList(),
        };
      },
      addCommands() {
        return {
          setTag:
            () =>
            ({ commands }) => {
              return commands.setMark(tag.name);
            },
          toggleTag:
            (attributes) =>
            ({ commands, tr, state, dispatch }) => {
              return commands.toggleMark(attributes.name, attributes);
            },
          unsetTag:
            () =>
            ({ commands }) => {
              return commands.unsetMark(tag.name);
            },
        };
      },
      addAttributes() {
        // Return an object with attribute configuration
        return {
          tag: {
            default: {},
          },
          annotationId: {
            default: null,
          },
          color: {
            default: tag.color,
            parseHTML: (element) => {
              return [
                {
                  color:
                    element.getAttribute("data-color") ||
                    element.style.backgroundColor,
                },
                {
                  tag: tag.name,
                },
              ];
            },
            renderHTML: (attributes) => {
              if (!attributes.color) {
                return {};
              }

              return {
                "data-color": attributes.color,
                style: `outline-color: ${attributes.color} !important; outline: 2px solid;`,
                class: "tag",
              };
            },
          },
        };
      },
      // parseHTML() {
      //   return [{ tag: 'a[href]' }]
      // },

      renderHTML({ HTMLAttributes }) {
        return [
          "a",
          mergeAttributes(this.options.HTMLAttributes, HTMLAttributes),
          0,
        ];
      },
    });
    tagExtensionArr.push(tempExtension);
  }

  return tagExtensionArr;
}
