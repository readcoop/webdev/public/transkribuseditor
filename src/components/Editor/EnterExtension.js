/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import { Extension, Plugin } from '@tiptap/core'

import { Extension } from '@tiptap/core'

const CustomExtension = Extension.create({
  // Your code here
})


// export default class EnterHandler extends Extension {

//   get name() {
//     return 'enter_handler'
//   }

//   get plugins() {
//     return [
//       new Plugin({
//         props: {
//           handleKeyDown: (view, event) => {
//             if (event.key === 'Enter' && !event.shiftKey) {
//               // do something
//               console.log('Enter')
//               return false
//             }

//             return false
//           },
//         },
//       }),
//     ]
//   }

// }