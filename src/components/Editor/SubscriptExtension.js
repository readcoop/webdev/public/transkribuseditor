/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import {
  Command,
  Mark,
  markInputRule,
  markPasteRule,
  mergeAttributes,
} from "@tiptap/core";

export const Subscript = Mark.create({
  name: "subscript",

  defaultOptions: {
    HTMLAttributes: {},
  },

  parseHTML() {
    return [
      {
        tag: "subscript",
      },
      {
        tag: "del",
      },
      {
        tag: "subscript",
      },
      {
        style: "text-decoration",
        consuming: false,
        getAttrs: (style) => (style.includes("line-through") ? {} : false),
      },
    ];
  },

  renderHTML({ HTMLAttributes }) {
    return [
      "sub",
      mergeAttributes(this.options.HTMLAttributes, HTMLAttributes),
      0,
    ];
  },

  addCommands() {
    return {
      setSub:
        () =>
        ({ commands }) => {
          return commands.setMark("subscript");
        },
      toggleSub:
        () =>
        ({ commands }) => {
          return commands.toggleMark("subscript");
        },
      unsetSub:
        () =>
        ({ commands }) => {
          return commands.unsetMark("subscript");
        },
    };
  },
});
