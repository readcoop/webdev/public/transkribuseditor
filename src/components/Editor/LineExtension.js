/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import { Node, mergeAttributes } from "@tiptap/core";
import { VueNodeViewRenderer } from "@tiptap/vue-2";
import Component from "./LineNode.vue";
import { TextSelection } from "prosemirror-state";
import { Plugin, PluginKey } from "prosemirror-state";
import store from "@/store";

export default Node.create({
  name: "line",

  group: "block+",

  // content: '(inline* | tag*)',
  content: "inline* ",
  addAttributes() {
    return {
      line: {
        default: [],
      },
    };
  },
  addProseMirrorPlugins() {
    return [
      new Plugin({
        props: {
          handleKeyDown: (view, event) => {
            if (event.which === 8 || event.which === 46) {
              view.state.deleting = true;
            }

            return false;
          },
        },

        filterTransaction: (transaction, state) => {
          var isDeleting = state.deleting;

          if (!state.deleting) {
            return true;
          }

          var result = true;

          transaction.mapping.maps.forEach((map) => {
            map.forEach((oldStart, oldEnd, newStart, newEnd) => {
              state.doc.nodesBetween(
                oldStart,
                oldEnd,
                (node, number, pos, parent, index) => {
                  if (node.type.name === "line") {
                    result = false;
                  }
                }
              );
            });
          });

          return result;
        },
      }),
    ];
  },
  addKeyboardShortcuts() {
    return {
      Enter: (state, dispatch, view) => {
        this.editor.commands.selectParentNode();
        var selection = this.editor.view.state.selection;
        var node = this.editor.view.state.doc.nodeAt(selection.$head.pos);
        this.editor.chain().selectNodeForward();
        if (node) {
          if (node.attrs.line) {
            this.editor.commands.setTextSelection(
              this.editor.state.selection.$head.pos + node.content.size + 1
            );
            var lineId = node.attrs.line.attributes.id;
            const el = document.getElementById(lineId);
            el.scrollIntoView({ behavior: "smooth" });
            store.dispatch("content/select", {
              target: lineId,
              origin: "content",
            });
          }
        }

        return true;
      },
      ArrowDown: (state, dispatch, view) => {
        this.editor.commands.selectParentNode();
        var selection = this.editor.view.state.selection;
        var node = this.editor.view.state.doc.nodeAt(selection.$head.pos);
        this.editor.chain().selectNodeForward();
        if (node) {
          if (node.attrs.line) {
            this.editor.commands.setTextSelection(
              this.editor.state.selection.$head.pos + node.content.size + 1
            );
            var lineId = node.attrs.line.attributes.id;
            const el = document.getElementById(lineId);
            el.scrollIntoView({ behavior: "smooth" });
            store.dispatch("content/select", {
              target: lineId,
              origin: "content",
            });
          }
        }

        return true;
      },
      ArrowUp: (state, dispatch, view) => {
        this.editor.commands.selectParentNode();
        var selection = this.editor.view.state.selection;
        var node = this.editor.view.state.doc.nodeAt(selection.$head.pos);
        if (node && node.attrs.line) {
          store.dispatch("content/select", {
            target: node.attrs.line.attributes.id,
            origin: "content",
          });
        }
      },
    };
  },

  parseHTML() {
    return [
      {
        tag: "div",
      },
    ];
  },

  renderHTML({ HTMLAttributes }) {
    return ["div", mergeAttributes(HTMLAttributes)];
  },

  addNodeView() {
    return VueNodeViewRenderer(Component);
  },
});
