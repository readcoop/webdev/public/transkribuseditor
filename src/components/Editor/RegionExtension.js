/**
This file is part of a project licensed under the GNU General Public License v3.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import { Node, mergeAttributes } from '@tiptap/core'
import { VueNodeViewRenderer } from '@tiptap/vue-2'
import Component from './RegionNode.vue'
import { Plugin, PluginKey } from 'prosemirror-state'


export default Node.create({
  name: 'region',

  group: 'block',
  selectable: false,
  content: '(line+ | table+)',  

  addAttributes() {
    return {
      lines: {
        default: [],
      },
      number: {
        default: 0,
      },
    }
  },
  addProseMirrorPlugins() {
    return [
      new Plugin({
        props: {
          handleKeyDown: (view, event) => {
            if (event.which === 8 || event.which === 46) {
              view.state.deleting = true
            }
            return false
          }
        },

        filterTransaction: (transaction, state) => {
          if (!state.deleting) {
            return true
          }

          var result = true
          let count = 0
          transaction.mapping.maps.forEach(map => {
            map.forEach((oldStart, oldEnd, newStart, newEnd) => {
              state.doc.nodesBetween(
                oldStart,
                oldEnd,
                (node, number, pos, parent, index) => {
                  if (node.type.name === 'region') {
                    count++
                  }
                }
              )
            })
          })
          if (count > 1) {
            result = false
          }
          return result
        }
      })
    ]
  },

  parseHTML() {
    return [
      {
        tag: 'div',
      },
    ]
  },

  renderHTML({ HTMLAttributes }) {
    return ['div', mergeAttributes(HTMLAttributes)]
  },

  addNodeView() {
    return VueNodeViewRenderer(Component)
  },
})