
# transkribus-editor

This texteditor based on TipTap is used by TranskribusLite and allows to transcribe pages in the PageXML format. In order to display the connection between
text line in the editor and image tht layout must have been recognized for the page. The image needs to be accessible via a IIIF URL.

## Integration

To integrate the editor in any webpage the component can be added as such:

```
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <link rel="icon" href="<%= BASE_URL %>favicon.ico" />
    <title>Transkribus Text-Editor</title>
  </head>
  <body>
    <noscript>
      <strong
        >We're sorry but vue doesn't work properly without JavaScript enabled.
        Please enable it to continue.</strong
      >
    </noscript>
    <div id="transkribusEditor" ref="editor"></div>
  </body>
</html>
```

Without any data attributes a demo IIIF URL and XML is displayed. Pass data attribute data-iiif-url to transmit IIIF info json to the image and the XML via the data-xml-json-string attribute. Furthermore pass a tag-configuration to use customized tags (see tag-config.json for required schema):

```
 <div id=transkribusEditor data-tags='{
                "definitions": {
                  "annotations": [
                    {
                      "name": "Testing",
                      "label": "Testing",
                      "attributes": ["expansion"],
                      "extra": [
                        { "name": "test", "label": "Test attribute", "type": "string" }
                      ],
                      "color": "#ff0000",
                      "icon": 8228
                    }]}}' ref=editor></div>

```

The following example shows how to pass an IIIF-URL and the according XML-JSON as a string (see entire example in full-example.html):

```
<div id="transkribusEditor" data-iiif-url="https://files.transkribus.eu/iiif/2/QJDYEHKUDZIDNBCYBYXBLAZT/info.json" data-xml-json-string='

            {
                "declaration": {
                  "attributes": { "version": "1.0", "encoding": "UTF-8", "standalone": "yes" }
              ....

            ' </div>
```

## Output

The output is set to the data attribute of <div id="transkribusEditor" data-output="...." > and also to the window object as window.output
An event bus instance listens to the 'dataSaved' event and from there the output object with XML and XMLJson can be further porcessed.

```
<script>
  window.eventBus.$on('dataSaved', function (data) {
    console.log("react on saving the data ");
    console.log(data);
  });
</script>
```

## Split up image and editor

If you want to split up text editor and image this would be one way to go:

```
 <!DOCTYPE html>
<html lang=en>

<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <link rel=icon href=/favicon.ico>
    <title>Transkribus Text-Editor</title>
    <link href=./css/app.c57368ba.css rel=preload as=style>
    <link href=./css/chunk-vendors.b7d36435.css rel=preload as=style>
    <link href=./js/app.2f9ff8fa.js rel=preload as=script>
    <link href=./js/chunk-vendors.03bdc520.js rel=preload as=script>
    <link href=./css/chunk-vendors.b7d36435.css rel=stylesheet>
    <link href=./css/app.c57368ba.css rel=stylesheet>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
</head>

<body><noscript><strong>We're sorry but vue doesn't work properly without JavaScript enabled. Please enable it to
    continue.</strong></noscript>
<div class="columns">
<div class="column">
    <div id=transkribusEditor data-tags='{
        "definitions": {
          "annotations": [
            {
              "name": "Testing",
              "label": "Ok",
              "attributes": ["expansion"],
              "extra": [
                { "name": "expansion", "label": "Was soll das", "type": "string" }
              ],
              "color": "#ff0000",
              "icon": 8228
            }]}}' ref=editor></div>
</div>
<div class="column">
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">Text Editor</a></li>
            <li><a href="#tabs-2">Configuration</a></li>
            <li><a href="#tabs-3">Metadata</a></li>
        </ul>
        <div id="tabs-1">
            <div id="move"></div>
        </div>
        <div id="tabs-2">
            <h1>Configuration</h1>
            <p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus
                id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus
                eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse
                sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing.
                Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla.
                Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium
                posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et
                purus.</p>
        </div>
        <div id="tabs-3">
            <h1>Metadata</h1>
            <p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel
                vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante.
                Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula
                tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
            <p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus.
                Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit.
                Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id
                euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut
                sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula
                velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus.
                Vivamus a libero vitae lectus hendrerit hendrerit.</p>
        </div>
    </div>
</div>
</div>
<script src=./js/chunk-vendors.03bdc520.js></script>
<script src=./js/app.2f9ff8fa.js></script>
<script>
$("#textEditor").appendTo($("#move"));
$(function () {
    $("#tabs").tabs();
});
$("#content").remove();
</script>
<style>
.tile {
    display: contents !important;

}
.hero .is-covering {
    position: fixed;
    left: 0;
    top: 0;
    width: 50%;
    height: 100%;
}
</style>
</body>

</html>
```
Integrate Transkribus Editor in any webpage , e.g.

```
<div id="app" data-iiif-url="https://files.transkribus.eu/iiif/2/QJDYEHKUDZIDNBCYBYXBLAZT/info.json" data-xml-json-string='
            
            {
                "declaration": {
                  "attributes": { "version": "1.0", "encoding": "UTF-8", "standalone": "yes" }
                },
                "elements": [
                  {
                    "type": "element",
                    "name": "PcGts",
                    "attributes": {
                      "xmlns": "http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15",
                      "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
                      "xsi:schemaLocation": "http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15 http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15/pagecontent.xsd"
                    },
                    "elements": [
                      {
                        "type": "element",
                        "name": "Metadata",
                        "elements": [
                          {
                            "type": "element",
                            "name": "Creator",
                            "elements": [
                              {
                                "type": "text",
                                "text": "prov=University of Rostock/Institute of Mathematics/CITlab|PLANET AI GmbH/Gundram Leifert/gundram.leifert@planet-ai.de:name=BBM Bulliot French C19th handwritten 2021(htr_id=30922)::::v=2.6.7\nprov=University of Rostock/Institute of Mathematics/CITlab|PLANET AI GmbH/Tobias Gruening/tobias.gruening@planet-ai.de:name=de.uros.citlab.module.baseline2polygon.B2PSeamMultiOriented:v=2.6.7\nprov=University of Rostock/Institute of Mathematics/CITlab/Gundram Leifert/gundram.leifert@uni-rostock.de:name=WrDiarium_newM4(htr_id=959)::chars.txt::v=?1.0.2\nprov=University of Rostock/Institute of Mathematics/CITlab/Tobias Gruening/tobias.gruening@uni-rostock.de:name=de.uro.citlab.module.baseline2polygon.B2PSeamMultiOriented:v=?1.0.1\nprov=University of Rostock/Institute of Mathematics/CITlab/Gundram Leifert/gundram.leifert@uni-rostock.de:name=net.sprnn:::(v=1.0.2)\nprov=University of Rostock/Institute of Mathematics/CITlab/Tobias Gruening/tobias.gruening@uni-rostock.de:name=de.uro.citlab.module.baseline2polygon.B2PSeamMultiOriented(v=1.0.1)\nprov=University of Rostock/Institute of Mathematics/CITlab/Gundram Leifert/gundram.leifert@uni-rostock.de:name=net.sprnn:::(v=1.0.2)\nprov=University of Rostock/Institute of Mathematics/CITlab/Tobias Gruening/tobias.gruening@uni-rostock.de:name=de.uro.citlab.module.baseline2polygon.B2PSeamMultiOriented(v=1.0.1)\n"
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "Created",
                            "elements": [
                              { "type": "text", "text": "2017-06-01T09:49:25.623+02:00" }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "LastChange",
                            "elements": [
                              { "type": "text", "text": "2021-08-03T10:41:31.873+02:00" }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "Comments",
                            "elements": [
                              {
                                "type": "text",
                                "text": "\n                Measurement unit: pixel\n                PrimaryLanguage: German\n                Language: GermanStandard\n                Language: OldGerman\n                Producer: ABBYY FineReader Engine 11"
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "TranskribusMetadata",
                            "attributes": {
                              "docId": "31740",
                              "pageId": "1110629",
                              "pageNr": "2",
                              "tsid": "1728202",
                              "status": "IN_PROGRESS",
                              "userId": "44",
                              "imgUrl": "https://dbis-thure.uibk.ac.at/f/Get?id=IAXXMHTBIXJKLZJDEMLLFRLA&fileType=view",
                              "xmlUrl": "https://dbis-thure.uibk.ac.at/f/Get?id=CNSOBMAXDDCIAGGIQMYJTMQS",
                              "imageId": "458489"
                            },
                            "elements": []
                          }
                        ]
                      },
                      {
                        "type": "element",
                        "name": "Page",
                        "attributes": {
                          "imageFilename": "553741.png",
                          "imageWidth": "1699",
                          "imageHeight": "2334"
                        },
                        "elements": [
                          {
                            "type": "element",
                            "name": "PrintSpace",
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 0, "y": 0 },
                                    { "x": 1699, "y": 0 },
                                    { "x": 1699, "y": 2334 },
                                    { "x": 0, "y": 2334 }
                                  ]
                                },
                                "elements": []
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "ReadingOrder",
                            "elements": [
                              {
                                "type": "element",
                                "name": "OrderedGroup",
                                "attributes": {
                                  "id": "ro_1627980091884",
                                  "caption": "Regions reading order"
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "RegionRefIndexed",
                                    "attributes": { "index": "0", "regionRef": "r_1_1" },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "RegionRefIndexed",
                                    "attributes": { "index": "3", "regionRef": "r_1_2" },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "RegionRefIndexed",
                                    "attributes": { "index": "5", "regionRef": "r_1_3" },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "RegionRefIndexed",
                                    "attributes": { "index": "6", "regionRef": "r_1_4" },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "RegionRefIndexed",
                                    "attributes": {
                                      "index": "7",
                                      "regionRef": "TextRegion_1501751574998_52"
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "RegionRefIndexed",
                                    "attributes": {
                                      "index": "8",
                                      "regionRef": "TextRegion_1501751574998_51"
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "RegionRefIndexed",
                                    "attributes": { "index": "9", "regionRef": "r_2_2" },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "RegionRefIndexed",
                                    "attributes": { "index": "10", "regionRef": "r_3_1" },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "RegionRefIndexed",
                                    "attributes": {
                                      "index": "11",
                                      "regionRef": "TextRegion_1501751602327_64"
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "RegionRefIndexed",
                                    "attributes": {
                                      "index": "12",
                                      "regionRef": "TextRegion_1505653819149_11"
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "RegionRefIndexed",
                                    "attributes": {
                                      "index": "13",
                                      "regionRef": "TextRegion_1505653819149_10"
                                    },
                                    "elements": []
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "TextRegion",
                            "attributes": {
                              "type": "header",
                              "id": "r_1_1",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 0 } },
                                { "name": "structure", "attributes": { "type": "header" } }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 62, "y": 82 },
                                    { "x": 1517, "y": 82 },
                                    { "x": 1517, "y": 157 },
                                    { "x": 62, "y": 157 }
                                  ]
                                },
                                "elements": []
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_1",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 0 } },
                                    {
                                      "name": "unclear",
                                      "attributes": { "offset": 22, "length": 8 }
                                    }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 55, "y": 141 },
                                        { "x": 1323, "y": 155 },
                                        { "x": 1526, "y": 139 },
                                        { "x": 1526, "y": 88 },
                                        { "x": 55, "y": 97 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 63, "y": 140 },
                                        { "x": 1516, "y": 140 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "AAN0 1732asd.  AVIN. E S PAANRISS"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": { "fontFamily": "Antiqua" },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextEquiv",
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Unicode",
                                    "elements": [
                                      {
                                        "type": "text",
                                        "text": "AAN0 1732.  AVIN. E S PAANRISS"
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "SeparatorRegion",
                            "attributes": {
                              "id": "r_7",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 1 } }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 69, "y": 158 },
                                    { "x": 1524, "y": 158 },
                                    { "x": 69, "y": 170 }
                                  ]
                                },
                                "elements": []
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "GraphicRegion",
                            "attributes": {
                              "id": "Graphic_1503924878009_5",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 2 } }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 55, "y": 189 },
                                    { "x": 342, "y": 189 },
                                    { "x": 342, "y": 360 },
                                    { "x": 55, "y": 360 }
                                  ]
                                },
                                "elements": []
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "TextRegion",
                            "attributes": {
                              "type": "header",
                              "id": "r_1_2",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 3 } },
                                { "name": "structure", "attributes": { "type": "header" } }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 417, "y": 173 },
                                    { "x": 1176, "y": 169 },
                                    { "x": 1185, "y": 344 },
                                    { "x": 415, "y": 344 }
                                  ]
                                },
                                "elements": []
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "line_1501751504639_50",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 0 } },
                                    {
                                      "name": "supplied",
                                      "attributes": {
                                        "offset": 12,
                                        "length": 5,
                                        "reason": "because"
                                      }
                                    }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 560, "y": 244 },
                                        { "x": 932, "y": 260 },
                                        { "x": 1019, "y": 228 },
                                        { "x": 1020, "y": 186 },
                                        { "x": 560, "y": 182 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 569, "y": 231 },
                                        { "x": 1011, "y": 232 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          { "type": "text", "text": "BienneVif.f eBwws" }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_2",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 1 } },
                                    {
                                      "name": "supplied",
                                      "attributes": {
                                        "offset": 0,
                                        "length": 8,
                                        "reason": "because"
                                      }
                                    }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 407, "y": 341 },
                                        { "x": 1194, "y": 338 },
                                        { "x": 1126, "y": 274 },
                                        { "x": 1037, "y": 318 },
                                        { "x": 841, "y": 325 },
                                        { "x": 768, "y": 281 },
                                        { "x": 715, "y": 316 },
                                        { "x": 652, "y": 274 },
                                        { "x": 600, "y": 325 },
                                        { "x": 407, "y": 287 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 416, "y": 340 },
                                        { "x": 1184, "y": 340 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [{ "type": "text", "text": "C1A4 e 14" }]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": { "fontFamily": "Antiqua" },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextEquiv",
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Unicode",
                                    "elements": [
                                      { "type": "text", "text": "BienneVif.f eB\nC1A4 e 14" }
                                    ]
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "GraphicRegion",
                            "attributes": {
                              "id": "Graphic_1503924883820_6",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 4 } }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 1225, "y": 190 },
                                    { "x": 1513, "y": 190 },
                                    { "x": 1513, "y": 349 },
                                    { "x": 1225, "y": 349 }
                                  ]
                                },
                                "elements": []
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "TextRegion",
                            "attributes": {
                              "type": "header",
                              "id": "r_1_3",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 5 } },
                                { "name": "structure", "attributes": { "type": "header" } }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 178, "y": 360 },
                                    { "x": 1395, "y": 360 },
                                    { "x": 1395, "y": 416 },
                                    { "x": 178, "y": 416 }
                                  ]
                                },
                                "elements": []
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_3",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 0 } },
                                    {
                                      "name": "blackening",
                                      "attributes": { "offset": 36, "length": 9 }
                                    }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 170, "y": 405 },
                                        { "x": 1403, "y": 402 },
                                        { "x": 1403, "y": 369 },
                                        { "x": 1321, "y": 361 },
                                        { "x": 170, "y": 365 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 179, "y": 404 },
                                        { "x": 1394, "y": 404 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "ENiC SOUeI ROmit. BAVPCLL. LND ont. EtPASeRLC SICIBCIE. ont  out wass"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextEquiv",
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Unicode",
                                    "elements": [
                                      {
                                        "type": "text",
                                        "text": "ENiC SOUeI ROmit. BAVPCLL. LND CACGOL. EtPASeRLC SICIBCIE."
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "TextRegion",
                            "attributes": {
                              "type": "header",
                              "id": "r_1_4",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 6 } },
                                { "name": "structure", "attributes": { "type": "header" } }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 65, "y": 420 },
                                    { "x": 1526, "y": 420 },
                                    { "x": 1526, "y": 530 },
                                    { "x": 65, "y": 530 }
                                  ]
                                },
                                "elements": []
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_4",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 0 } },
                                    {
                                      "name": "person",
                                      "attributes": { "offset": 0, "length": 32 }
                                    },
                                    {
                                      "name": "date",
                                      "attributes": { "offset": 17, "length": 10 }
                                    },
                                    {
                                      "name": "date",
                                      "attributes": { "offset": 36, "length": 14 }
                                    },
                                    {
                                      "name": "person",
                                      "attributes": { "offset": 40, "length": 7 }
                                    }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 484 },
                                        { "x": 1535, "y": 467 },
                                        { "x": 1518, "y": 423 },
                                        { "x": 57, "y": 422 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 472 },
                                        { "x": 1525, "y": 472 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "Bu RNDen II Der SAVPertiCPen SDO5 . SDuehoouclerCg DCSen DOII SDO5."
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_5",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 1 } },
                                    {
                                      "name": "person",
                                      "attributes": { "offset": 26, "length": 3 }
                                    }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 318, "y": 522 },
                                        { "x": 1278, "y": 516 },
                                        { "x": 1184, "y": 473 },
                                        { "x": 318, "y": 475 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 327, "y": 518 },
                                        { "x": 1268, "y": 518 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "Batt. DauS UBen BeV SOBAnN NCIeI RSAII CPOICn."
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextEquiv",
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Unicode",
                                    "elements": [
                                      {
                                        "type": "text",
                                        "text": "Bu RNDen II Der SAVPertiCPen SDO5 . SDuehoouclerCg DCSen DOII SDO5.\nBatt. DauS UBen BeV SOBAnN NCIeI RSAII CPOICn."
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "TextRegion",
                            "attributes": {
                              "type": "heading",
                              "id": "TextRegion_1501751574998_52",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 7 } },
                                { "name": "structure", "attributes": { "type": "heading" } }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 776, "y": 591 },
                                    { "x": 63, "y": 591 },
                                    { "x": 63, "y": 545 },
                                    { "x": 776, "y": 545 }
                                  ]
                                },
                                "elements": []
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_6",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 0 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 175, "y": 588 },
                                        { "x": 681, "y": 584 },
                                        { "x": 653, "y": 549 },
                                        { "x": 175, "y": 542 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 185, "y": 585 },
                                        { "x": 672, "y": 585 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          { "type": "text", "text": "DiCAN S.SANIAI. 27220" }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextEquiv",
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Unicode",
                                    "elements": [
                                      { "type": "text", "text": "DiCAN S.SANIAI. 27220" }
                                    ]
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "TextRegion",
                            "attributes": {
                              "type": "paragraph",
                              "id": "TextRegion_1501751574998_51",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 8 } }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 776, "y": 901 },
                                    { "x": 63, "y": 901 },
                                    { "x": 63, "y": 591 },
                                    { "x": 776, "y": 591 }
                                  ]
                                },
                                "elements": []
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_7",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 0 } },
                                    {
                                      "name": "unclear",
                                      "attributes": { "offset": 13, "length": 11 }
                                    }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 181, "y": 639 },
                                        { "x": 765, "y": 631 },
                                        { "x": 783, "y": 586 },
                                        { "x": 181, "y": 588 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 189, "y": 631 },
                                        { "x": 773, "y": 631 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "BIMOII DON SE. DeCCmBOII AIS"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_8",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 1 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 203, "y": 677 },
                                        { "x": 783, "y": 672 },
                                        { "x": 783, "y": 639 },
                                        { "x": 712, "y": 630 },
                                        { "x": 203, "y": 635 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 211, "y": 676 },
                                        { "x": 774, "y": 676 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "SDII 3IBOND DCB BOBOn BCI DEI"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_9",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 2 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 197, "y": 721 },
                                        { "x": 785, "y": 717 },
                                        { "x": 785, "y": 683 },
                                        { "x": 197, "y": 679 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 207, "y": 720 },
                                        { "x": 775, "y": 720 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "BBétigneioung CDUPI INAIS"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_10",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 3 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 55, "y": 761 },
                                        { "x": 781, "y": 765 },
                                        { "x": 781, "y": 726 },
                                        { "x": 725, "y": 719 },
                                        { "x": 55, "y": 721 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 64, "y": 766 },
                                        { "x": 772, "y": 766 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "NBenDS In DeC DIOPON DOPS CADCIIEN"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_11",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 4 } },
                                    {
                                      "name": "unclear",
                                      "attributes": { "offset": 15, "length": 7 }
                                    }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 805 },
                                        { "x": 781, "y": 812 },
                                        { "x": 781, "y": 772 },
                                        { "x": 57, "y": 767 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 811 },
                                        { "x": 772, "y": 811 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "TOlOn. VCIDCI. IDOIDCV DIC DCIIEN"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_12",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 5 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 55, "y": 860 },
                                        { "x": 783, "y": 849 },
                                        { "x": 783, "y": 818 },
                                        { "x": 715, "y": 808 },
                                        { "x": 55, "y": 810 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 64, "y": 858 },
                                        { "x": 773, "y": 858 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "BEIILOi MIE DOI DOIDONON BSLOE CnPiPICS"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_13",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 6 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 903 },
                                        { "x": 146, "y": 902 },
                                        { "x": 146, "y": 874 },
                                        { "x": 57, "y": 865 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 903 },
                                        { "x": 137, "y": 903 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [{ "type": "text", "text": "IION." }]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextEquiv",
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Unicode",
                                    "elements": [
                                      {
                                        "type": "text",
                                        "text": "BIMOII DON SE. DeCCmBOII AIS\nSDII 3IBOND DCB BOBOn BCI DEI\nBBétigneioung CDUPI INAIS\nNBenDS In DeC DIOPON DOPS CADCIIEN\nTOlOn. VCIDCI. IDOIDCV DIC DCIIEN\nBEIILOi MIE DOI DOIDONON BSLOE CnPiPICS\nIION."
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "TextRegion",
                            "attributes": {
                              "type": "paragraph",
                              "id": "r_2_2",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 9 } }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 54, "y": 901 },
                                    { "x": 781, "y": 901 },
                                    { "x": 781, "y": 2088 },
                                    { "x": 54, "y": 2088 }
                                  ]
                                },
                                "elements": []
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_14",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 0 } },
                                    {
                                      "name": "sic",
                                      "attributes": { "offset": 7, "length": 6 }
                                    }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 102, "y": 951 },
                                        { "x": 779, "y": 942 },
                                        { "x": 765, "y": 900 },
                                        { "x": 102, "y": 903 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 112, "y": 947 },
                                        { "x": 771, "y": 947 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DI0 IR mn Deu SEiViBOn DOS SEanPeII."
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_15",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 1 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 996 },
                                        { "x": 783, "y": 991 },
                                        { "x": 783, "y": 947 },
                                        { "x": 57, "y": 949 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 992 },
                                        { "x": 774, "y": 992 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DiofiB. Dautes DCc SOCiCLLI BCCI"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_16",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 2 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 58, "y": 1037 },
                                        { "x": 781, "y": 1035 },
                                        { "x": 781, "y": 996 },
                                        { "x": 58, "y": 1004 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 68, "y": 1036 },
                                        { "x": 772, "y": 1036 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "AM .DOPI DiC DON DIO BSCIMIILBLEN"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_17",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 3 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 58, "y": 1082 },
                                        { "x": 783, "y": 1080 },
                                        { "x": 783, "y": 1046 },
                                        { "x": 646, "y": 1033 },
                                        { "x": 58, "y": 1042 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 67, "y": 1081 },
                                        { "x": 773, "y": 1081 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "Ien RBCit. SEADPeIt. DEAIORRI CICONOIA"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_18",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 4 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 1130 },
                                        { "x": 783, "y": 1117 },
                                        { "x": 739, "y": 1079 },
                                        { "x": 57, "y": 1082 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 1126 },
                                        { "x": 773, "y": 1126 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DRAgOnIenA EPeVClA Diole SABE BiNS"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_19",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 5 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 1170 },
                                        { "x": 783, "y": 1164 },
                                        { "x": 783, "y": 1133 },
                                        { "x": 558, "y": 1121 },
                                        { "x": 57, "y": 1130 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 65, "y": 1170 },
                                        { "x": 774, "y": 1170 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DUICI BOcDRIUBNLCS geNRODenEINLNI"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_20",
                                  "primaryLanguage": "English",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 6 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 55, "y": 1217 },
                                        { "x": 783, "y": 1215 },
                                        { "x": 783, "y": 1181 },
                                        { "x": 726, "y": 1168 },
                                        { "x": 55, "y": 1181 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 64, "y": 1216 },
                                        { "x": 774, "y": 1216 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "MeBI ABeC DOn OINEI DOiD S BIDeIICRON"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_21",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 7 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 1259 },
                                        { "x": 785, "y": 1259 },
                                        { "x": 785, "y": 1217 },
                                        { "x": 57, "y": 1217 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 1261 },
                                        { "x": 776, "y": 1261 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DImC AVP CIDiDNCRiPC RInDACII GO II"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_22",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 8 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 1309 },
                                        { "x": 785, "y": 1303 },
                                        { "x": 750, "y": 1261 },
                                        { "x": 57, "y": 1268 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 65, "y": 1307 },
                                        { "x": 775, "y": 1307 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "Um ALe DOB SaBe BinDVICS EmpPOnDCNC"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_23",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 9 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 1340 },
                                        { "x": 785, "y": 1345 },
                                        { "x": 785, "y": 1310 },
                                        { "x": 57, "y": 1309 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 1352 },
                                        { "x": 775, "y": 1352 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "CeMNoDon LDUIDigROn DanCI ABSURAIIEII"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_24",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 10 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 1398 },
                                        { "x": 783, "y": 1391 },
                                        { "x": 783, "y": 1358 },
                                        { "x": 699, "y": 1351 },
                                        { "x": 57, "y": 1340 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 1398 },
                                        { "x": 774, "y": 1398 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "Bie AVI) Un neVON C.gen PEI EIngEs"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_25",
                                  "primaryLanguage": "English",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 11 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 58, "y": 1445 },
                                        { "x": 785, "y": 1442 },
                                        { "x": 759, "y": 1398 },
                                        { "x": 58, "y": 1400 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 67, "y": 1444 },
                                        { "x": 775, "y": 1444 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "BenD.S ESMBE PamnIIII qu BiIIOnI MI"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_26",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 12 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 55, "y": 1497 },
                                        { "x": 785, "y": 1480 },
                                        { "x": 752, "y": 1442 },
                                        { "x": 55, "y": 1445 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 63, "y": 1489 },
                                        { "x": 775, "y": 1489 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "POnDCIBAIOn AlgemCinen EIOR NICDEAII"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_27",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 13 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 1533 },
                                        { "x": 708, "y": 1538 },
                                        { "x": 787, "y": 1526 },
                                        { "x": 787, "y": 1497 },
                                        { "x": 57, "y": 1500 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 1533 },
                                        { "x": 777, "y": 1533 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "Cn DOn DeImOngIOn CIAOII RSOICEI PONS"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_28",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 14 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 1577 },
                                        { "x": 785, "y": 1571 },
                                        { "x": 741, "y": 1531 },
                                        { "x": 57, "y": 1535 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 1579 },
                                        { "x": 776, "y": 1579 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "Deun AVig In gVOPeL UND UnAEMEINCL BC."
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_29",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 15 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 58, "y": 1632 },
                                        { "x": 767, "y": 1632 },
                                        { "x": 785, "y": 1582 },
                                        { "x": 58, "y": 1591 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 67, "y": 1625 },
                                        { "x": 775, "y": 1625 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "BOINAII DCS DOVNERMROn BDOISI NAII"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_30",
                                  "primaryLanguage": "English",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 16 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 1668 },
                                        { "x": 785, "y": 1666 },
                                        { "x": 785, "y": 1632 },
                                        { "x": 730, "y": 1622 },
                                        { "x": 57, "y": 1632 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 65, "y": 1670 },
                                        { "x": 776, "y": 1670 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DorgehenDer VCIDCI. MIE BEIDDBNIICEI"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_31",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 17 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 58, "y": 1723 },
                                        { "x": 785, "y": 1716 },
                                        { "x": 785, "y": 1679 },
                                        { "x": 712, "y": 1672 },
                                        { "x": 58, "y": 1670 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 67, "y": 1717 },
                                        { "x": 775, "y": 1717 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DIeDig CDCIO SSMBOIE MAICI AUS DEII"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_32",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 18 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 58, "y": 1759 },
                                        { "x": 785, "y": 1754 },
                                        { "x": 748, "y": 1716 },
                                        { "x": 58, "y": 1730 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 67, "y": 1760 },
                                        { "x": 776, "y": 1760 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "49. PE V. EI. ODPCVC ODEE CIN EOB."
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_33",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 19 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 58, "y": 1798 },
                                        { "x": 785, "y": 1798 },
                                        { "x": 715, "y": 1757 },
                                        { "x": 58, "y": 1765 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 68, "y": 1805 },
                                        { "x": 775, "y": 1805 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DDPCi IUND DesABIC Dem DIIICIDOC)."
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_34",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 20 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 1851 },
                                        { "x": 787, "y": 1851 },
                                        { "x": 768, "y": 1805 },
                                        { "x": 57, "y": 1805 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 1850 },
                                        { "x": 777, "y": 1850 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "Ren Deinc OetLBDen.) UND DemNAI"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_35",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 21 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 46, "y": 1909 },
                                        { "x": 752, "y": 1898 },
                                        { "x": 785, "y": 1860 },
                                        { "x": 46, "y": 1863 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 55, "y": 1896 },
                                        { "x": 775, "y": 1896 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "LnICL BepRimMenDON EROmDEIOnSNNO"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_36",
                                  "primaryLanguage": "English",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 22 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 1942 },
                                        { "x": 787, "y": 1938 },
                                        { "x": 725, "y": 1898 },
                                        { "x": 57, "y": 1898 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 1941 },
                                        { "x": 777, "y": 1941 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "Danven. TChaIt mIE DEM POIEmNICCI"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_37",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 23 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 60, "y": 1987 },
                                        { "x": 787, "y": 1984 },
                                        { "x": 743, "y": 1940 },
                                        { "x": 60, "y": 1951 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 69, "y": 1989 },
                                        { "x": 778, "y": 1989 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "gCfDenCN AmBIOLLNIPBOn SOBS G50s"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_38",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 24 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 2033 },
                                        { "x": 787, "y": 2033 },
                                        { "x": 787, "y": 1997 },
                                        { "x": 739, "y": 1989 },
                                        { "x": 57, "y": 1987 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 2035 },
                                        { "x": 778, "y": 2035 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "MNO OCBAlIen MOIDON. MOIBCVI OBEI"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_39",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 25 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 57, "y": 2077 },
                                        { "x": 788, "y": 2077 },
                                        { "x": 788, "y": 2046 },
                                        { "x": 723, "y": 2033 },
                                        { "x": 57, "y": 2033 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 66, "y": 2083 },
                                        { "x": 780, "y": 2083 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "Dem DorBeRONIOn BeBeimNIIS DeI BBes"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextEquiv",
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Unicode",
                                    "elements": [
                                      {
                                        "type": "text",
                                        "text": "DI0 IR mn Deu SEiViBOn DOS SEanPeII.\nDiofiB. Dautes DCc SOCiCLLI BCCI\nAM .DOPI DiC DON DIO BSCIMIILBLEN\nIen RBCit. SEADPeIt. DEAIORRI CICONOIA\nDRAgOnIenA EPeVClA Diole SABE BiNS\nDUICI BOcDRIUBNLCS geNRODenEINLNI\nMeBI ABeC DOn OINEI DOiD S BIDeIICRON\nDImC AVP CIDiDNCRiPC RInDACII GO II\nUm ALe DOB SaBe BinDVICS EmpPOnDCNC\nCeMNoDon LDUIDigROn DanCI ABSURAIIEII\nBie AVI) Un neVON C.gen PEI EIngEs\nBenD.S ESMBE PamnIIII qu BiIIOnI MI\nPOnDCIBAIOn AlgemCinen EIOR NICDEAII\nCn DOn DeImOngIOn CIAOII RSOICEI PONS\nDeun AVig In gVOPeL UND UnAEMEINCL BC.\nBOINAII DCS DOVNERMROn BDOISI NAII\nDorgehenDer VCIDCI. MIE BEIDDBNIICEI\nDIeDig CDCIO SSMBOIE MAICI AUS DEII\n49. PE V. EI. ODPCVC ODEE CIN EOB.\nDDPCi IUND DesABIC Dem DIIICIDOC).\nRen Deinc OetLBDen.) UND DemNAI\nLnICL BepRimMenDON EROmDEIOnSNNO\nDanven. TChaIt mIE DEM POIEmNICCI\ngCfDenCN AmBIOLLNIPBOn SOBS G50s\nMNO OCBAlIen MOIDON. MOIBCVI OBEI\nDem DorBeRONIOn BeBeimNIIS DeI BBes"
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "TextRegion",
                            "attributes": {
                              "type": "paragraph",
                              "id": "r_3_1",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 10 } }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 814, "y": 539 },
                                    { "x": 1534, "y": 539 },
                                    { "x": 1534, "y": 950 },
                                    { "x": 814, "y": 950 }
                                  ]
                                },
                                "elements": []
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_40",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 0 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 812, "y": 586 },
                                        { "x": 1537, "y": 579 },
                                        { "x": 1537, "y": 542 },
                                        { "x": 1467, "y": 537 },
                                        { "x": 812, "y": 538 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 821, "y": 582 },
                                        { "x": 1528, "y": 582 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "RMneIOUng UnPEIS DCLNSI MiE DeIS"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_41",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 1 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 808, "y": 626 },
                                        { "x": 1538, "y": 621 },
                                        { "x": 1517, "y": 582 },
                                        { "x": 808, "y": 584 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 818, "y": 627 },
                                        { "x": 1529, "y": 627 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "BorgenCn EAmDOn CNIeVCNICn SEIIC."
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_42",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 2 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 808, "y": 670 },
                                        { "x": 1537, "y": 670 },
                                        { "x": 1464, "y": 631 },
                                        { "x": 808, "y": 626 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 817, "y": 671 },
                                        { "x": 1528, "y": 671 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "dEAmonI DiC RBOVE DOS.STCIL. IOENNIS 3."
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_43",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 3 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 808, "y": 710 },
                                        { "x": 1526, "y": 725 },
                                        { "x": 1542, "y": 672 },
                                        { "x": 808, "y": 683 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 817, "y": 715 },
                                        { "x": 1533, "y": 715 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "V.I6. DON DénenS. CENSCINI DIS AV5"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_44",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 4 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 808, "y": 761 },
                                        { "x": 1538, "y": 757 },
                                        { "x": 1538, "y": 726 },
                                        { "x": 808, "y": 717 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 817, "y": 761 },
                                        { "x": 1530, "y": 761 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DEmEOID. 2IIIAI I In DCI SOBe DOIOCS"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_45",
                                  "primaryLanguage": "English",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 5 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 808, "y": 808 },
                                        { "x": 1538, "y": 805 },
                                        { "x": 1509, "y": 765 },
                                        { "x": 1090, "y": 788 },
                                        { "x": 808, "y": 761 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 817, "y": 806 },
                                        { "x": 1530, "y": 806 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DalLen MAIen. NI150 B.I BDEC DiC"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_46",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 6 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 807, "y": 850 },
                                        { "x": 1540, "y": 847 },
                                        { "x": 1444, "y": 810 },
                                        { "x": 807, "y": 810 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 815, "y": 851 },
                                        { "x": 1531, "y": 851 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "RDOtt DeliCBEI D4B et Pêinen CIIBC."
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_47",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 7 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 807, "y": 896 },
                                        { "x": 1538, "y": 894 },
                                        { "x": 1538, "y": 861 },
                                        { "x": 1464, "y": 856 },
                                        { "x": 807, "y": 852 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 815, "y": 895 },
                                        { "x": 1530, "y": 895 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "BOLNON GOBN DLTCI DAmiE ECiNEE"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_48",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 8 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 808, "y": 942 },
                                        { "x": 1540, "y": 944 },
                                        { "x": 1484, "y": 900 },
                                        { "x": 808, "y": 900 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 817, "y": 940 },
                                        { "x": 1531, "y": 940 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "De An iOMC AtAIDEI DCIIOICII EDEIDC."
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextEquiv",
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Unicode",
                                    "elements": [
                                      {
                                        "type": "text",
                                        "text": "RMneIOUng UnPEIS DCLNSI MiE DeIS\nBorgenCn EAmDOn CNIeVCNICn SEIIC.\ndEAmonI DiC RBOVE DOS.STCIL. IOENNIS 3.\nV.I6. DON DénenS. CENSCINI DIS AV5\nDEmEOID. 2IIIAI I In DCI SOBe DOIOCS\nDalLen MAIen. NI150 B.I BDEC DiC\nRDOtt DeliCBEI D4B et Pêinen CIIBC.\nBOLNON GOBN DLTCI DAmiE ECiNEE\nDe An iOMC AtAIDEI DCIIOICII EDEIDC."
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "TextRegion",
                            "attributes": {
                              "type": "paragraph",
                              "id": "TextRegion_1501751602327_64",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 11 } }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 1539, "y": 2084 },
                                    { "x": 814, "y": 2084 },
                                    { "x": 814, "y": 947 },
                                    { "x": 1539, "y": 947 }
                                  ]
                                },
                                "elements": []
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_49",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 0 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 852, "y": 989 },
                                        { "x": 1542, "y": 984 },
                                        { "x": 1528, "y": 945 },
                                        { "x": 852, "y": 945 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 862, "y": 985 },
                                        { "x": 1533, "y": 985 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DonnenRAgIDONL. BEINCL I722 1 018"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_50",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 1 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 810, "y": 1033 },
                                        { "x": 1486, "y": 1042 },
                                        { "x": 1544, "y": 995 },
                                        { "x": 810, "y": 996 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 819, "y": 1031 },
                                        { "x": 1534, "y": 1031 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "Am DEOV.SABVO.S A9 MAVen DiC DEMSDNS"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_51",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 2 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 810, "y": 1077 },
                                        { "x": 1544, "y": 1071 },
                                        { "x": 1502, "y": 1033 },
                                        { "x": 810, "y": 1033 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 820, "y": 1077 },
                                        { "x": 1534, "y": 1077 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "IeBe CLiCEDIinPguNgOn BOV DOn DRORICS"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_52",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 3 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 808, "y": 1122 },
                                        { "x": 1540, "y": 1124 },
                                        { "x": 1540, "y": 1084 },
                                        { "x": 1467, "y": 1073 },
                                        { "x": 808, "y": 1082 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 818, "y": 1121 },
                                        { "x": 1532, "y": 1121 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "IOnDS UnD BSOLIViILIBIS SEAVPEII. LOPEI"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_53",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 4 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 808, "y": 1168 },
                                        { "x": 1425, "y": 1177 },
                                        { "x": 1544, "y": 1163 },
                                        { "x": 1544, "y": 1133 },
                                        { "x": 808, "y": 1135 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 818, "y": 1166 },
                                        { "x": 1534, "y": 1166 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "ABgele9E IVOIDON. DAnN BABOn PCB RSOIS"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_54",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 5 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 810, "y": 1214 },
                                        { "x": 1542, "y": 1208 },
                                        { "x": 1517, "y": 1168 },
                                        { "x": 810, "y": 1175 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 819, "y": 1212 },
                                        { "x": 1533, "y": 1212 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "MILIAS DiC DCAICrOnD. SEAVPeLI. UND.SEOS"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_55",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 6 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 810, "y": 1259 },
                                        { "x": 1542, "y": 1259 },
                                        { "x": 1542, "y": 1225 },
                                        { "x": 1482, "y": 1215 },
                                        { "x": 810, "y": 1221 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 819, "y": 1257 },
                                        { "x": 1533, "y": 1257 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "NOL. CERIDoIiI.VC ARAIeRRIenI IAMIDEII"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_56",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 7 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 810, "y": 1305 },
                                        { "x": 1544, "y": 1303 },
                                        { "x": 1509, "y": 1259 },
                                        { "x": 810, "y": 1259 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 820, "y": 1302 },
                                        { "x": 1534, "y": 1302 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "UcAIeLIigROn CIEIBErRORinNOn II"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_57",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 8 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 810, "y": 1349 },
                                        { "x": 1544, "y": 1349 },
                                        { "x": 1544, "y": 1314 },
                                        { "x": 1484, "y": 1303 },
                                        { "x": 810, "y": 1305 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 820, "y": 1347 },
                                        { "x": 1534, "y": 1347 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "BeOteIIUng DOS DeuN BSOnCLAnIPDeII"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_58",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 9 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 810, "y": 1394 },
                                        { "x": 1544, "y": 1394 },
                                        { "x": 1511, "y": 1349 },
                                        { "x": 1455, "y": 1376 },
                                        { "x": 810, "y": 1349 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 819, "y": 1392 },
                                        { "x": 1534, "y": 1392 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "BOIIIAPCrE CLV. DOICI PInti. MII"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_59",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 10 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 812, "y": 1438 },
                                        { "x": 1546, "y": 1436 },
                                        { "x": 1546, "y": 1405 },
                                        { "x": 1469, "y": 1392 },
                                        { "x": 812, "y": 1394 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 821, "y": 1439 },
                                        { "x": 1536, "y": 1439 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DenCndEIIICIII DOSgOIDOnCn RSIICP.BIS5A95"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_60",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 11 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 812, "y": 1495 },
                                        { "x": 1546, "y": 1478 },
                                        { "x": 1462, "y": 1440 },
                                        { "x": 812, "y": 1442 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 822, "y": 1484 },
                                        { "x": 1536, "y": 1484 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "PeIt.BeIIOn BOBCimOn RROGIOnI CAIS"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_61",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 12 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 810, "y": 1531 },
                                        { "x": 1517, "y": 1538 },
                                        { "x": 1546, "y": 1489 },
                                        { "x": 810, "y": 1495 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 820, "y": 1530 },
                                        { "x": 1536, "y": 1530 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "MerCmnO CAOLiCVOnI MIC AVII LOES"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_62",
                                  "primaryLanguage": "English",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 13 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 812, "y": 1577 },
                                        { "x": 1544, "y": 1571 },
                                        { "x": 1544, "y": 1540 },
                                        { "x": 1478, "y": 1531 },
                                        { "x": 812, "y": 1531 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 821, "y": 1575 },
                                        { "x": 1535, "y": 1575 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "Damon) In OB9emCIDIOS SADPELL. DIOI"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_63",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 14 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 810, "y": 1628 },
                                        { "x": 1544, "y": 1622 },
                                        { "x": 1544, "y": 1584 },
                                        { "x": 1405, "y": 1575 },
                                        { "x": 810, "y": 1577 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 820, "y": 1621 },
                                        { "x": 1535, "y": 1621 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "PeBS.DAVBCIRoBOnI UND DAPOIBPOI DeM"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_64",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 15 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 812, "y": 1661 },
                                        { "x": 1544, "y": 1666 },
                                        { "x": 1544, "y": 1633 },
                                        { "x": 1486, "y": 1622 },
                                        { "x": 812, "y": 1628 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 822, "y": 1666 },
                                        { "x": 1535, "y": 1666 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "GOnCs. DienR AbgeMAILCCI NêlCPCI"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_65",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 16 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 812, "y": 1716 },
                                        { "x": 1544, "y": 1710 },
                                        { "x": 1524, "y": 1666 },
                                        { "x": 967, "y": 1664 },
                                        { "x": 923, "y": 1692 },
                                        { "x": 812, "y": 1664 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 821, "y": 1713 },
                                        { "x": 1534, "y": 1713 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "BDIO DOIPiIRL GnADen DOS BCit."
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_66",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 17 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 812, "y": 1759 },
                                        { "x": 1535, "y": 1754 },
                                        { "x": 1484, "y": 1716 },
                                        { "x": 812, "y": 1716 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 822, "y": 1757 },
                                        { "x": 1525, "y": 1757 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "EOm. ERCLDB SSLIP I UND OICinRIILS"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_67",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 18 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 812, "y": 1814 },
                                        { "x": 1542, "y": 1803 },
                                        { "x": 1542, "y": 1768 },
                                        { "x": 1447, "y": 1757 },
                                        { "x": 812, "y": 1767 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 821, "y": 1802 },
                                        { "x": 1533, "y": 1802 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "S4 RBienn DOn GIOmUnO GIP DON"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_68",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 19 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 814, "y": 1851 },
                                        { "x": 1478, "y": 1856 },
                                        { "x": 1546, "y": 1847 },
                                        { "x": 1546, "y": 1807 },
                                        { "x": 814, "y": 1812 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 823, "y": 1847 },
                                        { "x": 1536, "y": 1847 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "SSONOnIEIiS DerNiCRLCL. SDL XRaieRAI"
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_69",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 20 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 812, "y": 1893 },
                                        { "x": 1546, "y": 1889 },
                                        { "x": 1546, "y": 1858 },
                                        { "x": 1460, "y": 1847 },
                                        { "x": 1230, "y": 1874 },
                                        { "x": 812, "y": 1851 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 822, "y": 1893 },
                                        { "x": 1536, "y": 1893 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DiC BSCmMiIBIC EOm. LantéIin RImAS"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_70",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 21 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 814, "y": 1942 },
                                        { "x": 1546, "y": 1935 },
                                        { "x": 1469, "y": 1893 },
                                        { "x": 814, "y": 1894 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 823, "y": 1938 },
                                        { "x": 1536, "y": 1938 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "1IA 2BIIReLinA ABeII BaBOn neBR DEIO"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_71",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 22 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 814, "y": 1989 },
                                        { "x": 1546, "y": 1984 },
                                        { "x": 1546, "y": 1947 },
                                        { "x": 1436, "y": 1938 },
                                        { "x": 814, "y": 1942 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 823, "y": 1984 },
                                        { "x": 1537, "y": 1984 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "DUnChleuchLigROn CIBS.DeCROgINIn DCI"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_72",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 23 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 814, "y": 2033 },
                                        { "x": 1498, "y": 2035 },
                                        { "x": 1546, "y": 1997 },
                                        { "x": 814, "y": 1998 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 823, "y": 2030 },
                                        { "x": 1537, "y": 2030 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "ACRSemiPiGen SEIICCS. I.Dem BOIIO8."
                                          }
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "tl_73",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 24 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 816, "y": 2081 },
                                        { "x": 1548, "y": 2073 },
                                        { "x": 1548, "y": 2039 },
                                        { "x": 1420, "y": 2029 },
                                        { "x": 991, "y": 2059 },
                                        { "x": 816, "y": 2033 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 824, "y": 2077 },
                                        { "x": 1538, "y": 2077 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [
                                          {
                                            "type": "text",
                                            "text": "ientI NêlegOn BBIC DOCDNLIDenI"
                                          }
                                        ]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextEquiv",
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Unicode",
                                    "elements": [
                                      {
                                        "type": "text",
                                        "text": "DonnenRAgIDONL. BEINCL I722 1 018\nAm DEOV.SABVO.S A9 MAVen DiC DEMSDNS\nIeBe CLiCEDIinPguNgOn BOV DOn DRORICS\nIOnDS UnD BSOLIViILIBIS SEAVPEII. LOPEI\nABgele9E IVOIDON. DAnN BABOn PCB RSOIS\nMILIAS DiC DCAICrOnD. SEAVPeLI. UND.SEOS\nNOL. CERIDoIiI.VC ARAIeRRIenI IAMIDEII\nUcAIeLIigROn CIEIBErRORinNOn II\nBeOteIIUng DOS DeuN BSOnCLAnIPDeII\nBOIIIAPCrE CLV. DOICI PInti. MII\nDenCndEIIICIII DOSgOIDOnCn RSIICP.BIS5A95\nPeIt.BeIIOn BOBCimOn RROGIOnI CAIS\nMerCmnO CAOLiCVOnI MIC AVII LOES\nDamon) In OB9emCIDIOS SADPELL. DIOI\nPeBS.DAVBCIRoBOnI UND DAPOIBPOI DeM\nGOnCs. DienR AbgeMAILCCI NêlCPCI\nBDIO DOIPiIRL GnADen DOS BCit.\nEOm. ERCLDB SSLIP I UND OICinRIILS\nS4 RBienn DOn GIOmUnO GIP DON\nSSONOnIEIiS DerNiCRLCL. SDL XRaieRAI\nDiC BSCmMiIBIC EOm. LantéIin RImAS\n1IA 2BIIReLinA ABeII BaBOn neBR DEIO\nDUnChleuchLigROn CIBS.DeCROgINIn DCI\nACRSemiPiGen SEIICCS. I.Dem BOIIO8.\nientI NêlegOn BBIC DOCDNLIDenI"
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "TextRegion",
                            "attributes": {
                              "type": "signature-mark",
                              "id": "TextRegion_1505653819149_11",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 12 } },
                                {
                                  "name": "structure",
                                  "attributes": { "type": "signature-mark" }
                                }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 1190, "y": 2133 },
                                    { "x": 814, "y": 2133 },
                                    { "x": 814, "y": 2084 },
                                    { "x": 1190, "y": 2084 }
                                  ]
                                },
                                "elements": []
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "line_1505653819180_13",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 0 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 807, "y": 2133 },
                                        { "x": 1199, "y": 2126 },
                                        { "x": 807, "y": 2077 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 815, "y": 2125 },
                                        { "x": 1190, "y": 2125 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [{ "type": "text", "text": "de" }]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextEquiv",
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Unicode",
                                    "elements": [{ "type": "text", "text": "de" }]
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "type": "element",
                            "name": "TextRegion",
                            "attributes": {
                              "type": "catch-word",
                              "id": "TextRegion_1505653819149_10",
                              "custom": [
                                { "name": "readingOrder", "attributes": { "index": 13 } },
                                {
                                  "name": "structure",
                                  "attributes": { "type": "catch-word" }
                                }
                              ]
                            },
                            "elements": [
                              {
                                "type": "element",
                                "name": "Coords",
                                "attributes": {
                                  "points": [
                                    { "x": 1539, "y": 2133 },
                                    { "x": 1190, "y": 2133 },
                                    { "x": 1190, "y": 2084 },
                                    { "x": 1539, "y": 2084 }
                                  ]
                                },
                                "elements": []
                              },
                              {
                                "type": "element",
                                "name": "TextLine",
                                "attributes": {
                                  "id": "line_1505653819180_12",
                                  "primaryLanguage": "German",
                                  "custom": [
                                    { "name": "readingOrder", "attributes": { "index": 0 } }
                                  ]
                                },
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Coords",
                                    "attributes": {
                                      "points": [
                                        { "x": 1181, "y": 2126 },
                                        { "x": 1535, "y": 2124 },
                                        { "x": 1546, "y": 2088 },
                                        { "x": 1181, "y": 2110 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "Baseline",
                                    "attributes": {
                                      "points": [
                                        { "x": 1190, "y": 2125 },
                                        { "x": 1537, "y": 2125 }
                                      ]
                                    },
                                    "elements": []
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextEquiv",
                                    "elements": [
                                      {
                                        "type": "element",
                                        "name": "Unicode",
                                        "elements": [{ "type": "text", "text": "214" }]
                                      }
                                    ]
                                  },
                                  {
                                    "type": "element",
                                    "name": "TextStyle",
                                    "attributes": {
                                      "fontFamily": "Times New Roman",
                                      "fontSize": "8.5"
                                    },
                                    "elements": []
                                  }
                                ]
                              },
                              {
                                "type": "element",
                                "name": "TextEquiv",
                                "elements": [
                                  {
                                    "type": "element",
                                    "name": "Unicode",
                                    "elements": [{ "type": "text", "text": "214" }]
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
              
            ' </div>
```